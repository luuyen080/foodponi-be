package data;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Date;

public class InitializationData {

    public void insertAll(JdbcTemplate jdbcTemplate) {
        String insertUsers = "INSERT INTO users (id, created_date, avatar, email, password, role, status, username, address_id) " +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(insertUsers, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1", new Date(), "img.jpeg", "customer1@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "CUSTOMER", true, "customer1", null);
        jdbcTemplate.update(insertUsers, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2", new Date(), "img.jpeg", "retailer2@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "RETAILER", true, "retailer2", null);
        jdbcTemplate.update(insertUsers, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3", new Date(), "img.jpeg", "admin3@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "ADMIN", false, "admin3", null);
        jdbcTemplate.update(insertUsers, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4", new Date(), "img.jpeg", "retailer4@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "RETAILER", true, "retailer4", null);
        jdbcTemplate.update(insertUsers, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu5", new Date(), "img.jpeg", "customer5@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "CUSTOMER", true, "customer5", null);
        jdbcTemplate.update(insertUsers, "6d0bb5ac-3c1f-4092-96fe-5d27bfc50dc9", new Date(), "img.jpeg", "customer6@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "CUSTOMER", true, "customer6", null);
        jdbcTemplate.update(insertUsers, "87d87793-fa2c-44cf-8fe1-065d240458b5", new Date(), "img.jpeg", "admin7@gmail.com", "$2a$10$yC9zBxayraMCGrpx0e7Qd.F/RsxqzYtzPGyFquFqpU9KYwH8bVhom", "ADMIN", true, "admin7", null);

        String insertAddresses = "INSERT INTO addresses (id, created_date, full_name, phone_number, address, lon, lat, user_id) " +
                "VALUES (?,?,?,?,?,?,?, ?)";
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa1", new Date(), "Luu Thi Hai Yen", "0312343456", "123 Maple Avenue", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1");
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa2", new Date(), "Hai Yen", "0123456789", "456 Ward", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1");
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa3", new Date(), "Emily Johnson", "5555555555", "GHI Street, 456 Ward", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa4", new Date(), "Hai Yen", "0123456789", "789 Maple Avenue, GHI District", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3");
        jdbcTemplate.update(insertAddresses, "3fc53a4a-ba7e-45b3-89d3-c7e887c6e38e", new Date(), "Luu Thi Hai Yen", "0312343456", "PQR Province, GHI Street, 456 Ward", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1");
        jdbcTemplate.update(insertAddresses, "a3701501-1d80-41d2-85f2-512bb6da83eb", new Date(), "Emily Johnson", "5555555555", "457 Ward", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertAddresses, "d54f5b31-1764-451e-a1b9-4dd85a1fe5eb", new Date(), "Hai Yen", "0123456789", "788 Maple Avenue", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3");
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa5", new Date(), "Hai Yen", "0123456789", "789 Maple Avenue, GHI District", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1");
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa6", new Date(), "Hai Yen", "0123456789", "789 Maple Avenue, GHI District", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertAddresses, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa7", new Date(), "Hai Yen", "0123456789", "789 Maple Avenue, GHI District", 105.72105620969991, 21.04874565, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3");

        String updateUsers = "UPDATE users SET address_id = ? WHERE id = ?";
        jdbcTemplate.update(updateUsers, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa1", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1");
        jdbcTemplate.update(updateUsers, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa3", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(updateUsers, "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa4", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3");

        String insertCategories = "INSERT INTO categories (id, parent_id, category_name, slug, description, thumbnail, created_date) " +
                "VALUES (?,?,?,?,?,?,?)";
        jdbcTemplate.update(insertCategories, "cccccccc-cccc-cccc-cccc-ccccccccccc1", null, "C1", "c-1", null, null, new Date());
        jdbcTemplate.update(insertCategories, "cccccccc-cccc-cccc-cccc-ccccccccccc2", "cccccccc-cccc-cccc-cccc-ccccccccccc1", "C2", "c-2", null, null, new Date());
        jdbcTemplate.update(insertCategories, "cccccccc-cccc-cccc-cccc-ccccccccccc3", "cccccccc-cccc-cccc-cccc-ccccccccccc1", "C3", "c-3", null, null, new Date());
        jdbcTemplate.update(insertCategories, "cccccccc-cccc-cccc-cccc-ccccccccccc4", "cccccccc-cccc-cccc-cccc-ccccccccccc2", "C4", "c-4", null, null, new Date());
        jdbcTemplate.update(insertCategories, "cccccccc-cccc-cccc-cccc-ccccccccccc5", "cccccccc-cccc-cccc-cccc-ccccccccccc2", "C5", "c-5", null, null, new Date());
        jdbcTemplate.update(insertCategories, "cccccccc-cccc-cccc-cccc-ccccccccccc6", "cccccccc-cccc-cccc-cccc-ccccccccccc3", "C6", "c-6", null, null, new Date());

        String insertProducts = "INSERT INTO products (id, created_date, name, short_description, slug, status, thumbnail, user_id) " +
                "VALUES (?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(insertProducts, "ppppppp-pppp-pppp-pppp-ppppppppppp1", new Date(), "P1", "sdesc", "p-1", true, "img.jpeg", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertProducts, "ppppppp-pppp-pppp-pppp-ppppppppppp2", new Date(), "P2", "sdesc", "p-2", true, "img.jpeg", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertProducts, "ppppppp-pppp-pppp-pppp-ppppppppppp3", new Date(), "P3", "sdesc", "p-3", true, "img.jpeg", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertProducts, "ppppppp-pppp-pppp-pppp-ppppppppppp4", new Date(), "P4", "sdesc", "p-4", false, "img.jpeg", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2");
        jdbcTemplate.update(insertProducts, "ppppppp-pppp-pppp-pppp-ppppppppppp5", new Date(), "P5", "sdesc", "p-4", true, "img.jpeg", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4");

        String insertProductDetails = "INSERT INTO product_details (id, created_date, description, images, name, price, status, product_id) " +
                "VALUES (?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(insertProductDetails, "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1", new Date(), "desc", null, "PD1", 5, true, "ppppppp-pppp-pppp-pppp-ppppppppppp1");
        jdbcTemplate.update(insertProductDetails, "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp2", new Date(), "desc", null, "PD2", 10, true, "ppppppp-pppp-pppp-pppp-ppppppppppp1");
        jdbcTemplate.update(insertProductDetails, "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp3", new Date(), "desc", null, "PD3", 15, true, "ppppppp-pppp-pppp-pppp-ppppppppppp2");
        jdbcTemplate.update(insertProductDetails, "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp4", new Date(), "desc", null, "PD4", 20, false, "ppppppp-pppp-pppp-pppp-ppppppppppp3");
        jdbcTemplate.update(insertProductDetails, "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp5", new Date(), "desc", null, "PD5", 25, true, "ppppppp-pppp-pppp-pppp-ppppppppppp4");
        jdbcTemplate.update(insertProductDetails, "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp6", new Date(), "desc", null, "PD6", 25, true, "ppppppp-pppp-pppp-pppp-ppppppppppp5");

        String insertOrders = "INSERT INTO orders (id, created_date, note, payment, status, total_amount, user_id, shipping_address) " +
                "VALUES (?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(insertOrders, "oooooooo-oooo-oooo-oooo-ooooooooooo1", new Date(), null, "{\"id\":\"dazdxy\",\"method\":\"CASH\",\"status\":\"PAYING\"}", "PENDING", 20, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1", "{\"fullName\":\"Luu Thi Hai Yen\",\"phoneNumber\":\"0323452345\",\"address\":\"aaa Maple Avenue\"}");
        jdbcTemplate.update(insertOrders, "oooooooo-oooo-oooo-oooo-ooooooooooo2", new Date(), null, "{\"id\":\"dazabc\",\"method\":\"CASH\",\"status\":\"PAYING\"}", "PENDING", 5, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4", "{\"fullName\":\"Luu Thi Hai Yen\",\"phoneNumber\":\"0323452345\",\"address\":\"aaa Maple Avenue\"}");
        jdbcTemplate.update(insertOrders, "oooooooo-oooo-oooo-oooo-ooooooooooo3", new Date(), null, "{\"id\":\"dazabc\",\"method\":\"CASH\",\"status\":\"PAID\"}", "COMPLETED", 20, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1", "{\"fullName\":\"Luu Thi Hai Yen\",\"phoneNumber\":\"0323452345\",\"address\":\"aaa Maple Avenue\"}");

        String insertOrderItems = "INSERT INTO order_items (id, created_date, note, price, quantity, order_id, product_detail_id, rate) " +
                "VALUES (?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(insertOrderItems, "oioioioi-oioi-oioi-oioi-oioioioioio1", new Date(), null, 5, 1, "oooooooo-oooo-oooo-oooo-ooooooooooo1", "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1", null);
        jdbcTemplate.update(insertOrderItems, "oioioioi-oioi-oioi-oioi-oioioioioio2", new Date(), null, 15, 1, "oooooooo-oooo-oooo-oooo-ooooooooooo1", "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp3", null);
        jdbcTemplate.update(insertOrderItems, "oioioioi-oioi-oioi-oioi-oioioioioio3", new Date(), null, 5, 1, "oooooooo-oooo-oooo-oooo-ooooooooooo2", "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1", null);
        jdbcTemplate.update(insertOrderItems, "oioioioi-oioi-oioi-oioi-oioioioioio4", new Date(), null, 5, 1, "oooooooo-oooo-oooo-oooo-ooooooooooo3", "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1", null);
        jdbcTemplate.update(insertOrderItems, "oioioioi-oioi-oioi-oioi-oioioioioio5", new Date(), null, 15, 1, "oooooooo-oooo-oooo-oooo-ooooooooooo3", "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp3", "{\"rate\":5,\"message\":null,\"images\":null}");

        String insertFileUpload = "INSERT INTO file_uploads (id, name, extension, content_type, size, url, dimension, user_id, created_date)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(insertFileUpload, "ffffffff-ffff-ffff-ffff-ffffffffffff1", "file1", ".png", "image/png", 1789818L, "http://localhost:8080/upload/name1.png", null, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1", new Date());
        jdbcTemplate.update(insertFileUpload, "ffffffff-ffff-ffff-ffff-ffffffffffff2", "file2", ".jpg", "image/jpeg", 2000000L, "http://localhost:8080/upload/name2.jpg", null, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2", new Date());
        jdbcTemplate.update(insertFileUpload, "ffffffff-ffff-ffff-ffff-ffffffffffff3", "file3", ".txt", "text/plain", 100000L, "http://localhost:8080/upload/name3.txt", null, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3", new Date());
        jdbcTemplate.update(insertFileUpload, "ffffffff-ffff-ffff-ffff-ffffffffffff4", "file4", ".pdf", "application/pdf", 500000L, "http://localhost:8080/upload/name4.pdf", null, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1", new Date());
        jdbcTemplate.update(insertFileUpload, "ffffffff-ffff-ffff-ffff-ffffffffffff5", "file5", ".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 800000L, "http://localhost:8080/upload/name5.docx", null, "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2", new Date());
    }

    public void deleteAll(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.execute("SET FOREIGN_KEY_CHECKS=0;");
        jdbcTemplate.execute("DELETE FROM order_items");
        jdbcTemplate.execute("DELETE FROM orders");
        jdbcTemplate.execute("DELETE FROM product_details");
        jdbcTemplate.execute("DELETE FROM products");
        jdbcTemplate.execute("DELETE FROM categories");
        jdbcTemplate.execute("DELETE FROM addresses");
        jdbcTemplate.execute("DELETE FROM users");
        jdbcTemplate.execute("DELETE FROM file_uploads");
        jdbcTemplate.execute("SET FOREIGN_KEY_CHECKS=1;");
    }

}
