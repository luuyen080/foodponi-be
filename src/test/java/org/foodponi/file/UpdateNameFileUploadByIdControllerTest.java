package org.foodponi.file;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.file.FileUploadDTO;
import org.foodponi.business.file.enums.FileUploadErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UpdateNameFileUploadByIdControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    private FileUploadDTO.NameFileUploadRequest nameFileUploadRequest = new FileUploadDTO.NameFileUploadRequest();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);

        nameFileUploadRequest.setName("conghahaha");
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateNameFileUploadById_AsAdmin_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void updateNameFileUploadById_AsRetailer_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateNameFileUploadById_AsCustomer_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateNameFileUploadById_WithNameFileUploadIsNull_AsAdmin_ThrowsException() throws Exception {
        nameFileUploadRequest.setName(null);

        Map<String, Object> details = Map.of("name", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details
                )
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateNameFileUploadById_WithNameFileUploadIsEmpty_AsAdmin_ThrowsException() throws Exception {
        nameFileUploadRequest.setName("");

        Map<String, Object> details = Map.of("name", "must not be blank");
        ;

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details
                )
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateNameFileUploadById_WithFileUploadIdNotFound_AsAdmin_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(
                        FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND.getCode(),
                        FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND.getMessage(),
                        null
                )
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff30")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateNameFileUploadById_WithFileUploadFromAnotherUser_AsAdmin_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(
                        CommonErrorCode.ACCESS_FORBIDDEN.getCode(),
                        CommonErrorCode.ACCESS_FORBIDDEN.getMessage(),
                        null
                )
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void updateNameFileUploadById_AsAnonymousUser_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(nameFileUploadRequest)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}
