package org.foodponi.file;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UploadSingleFileControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    private MockMultipartFile multipartFile;

    @BeforeEach
    void setUp() throws IOException {
        initializationData.insertAll(jdbcTemplate);

        // Tạo một tệp giả mạo để tải lên
        String relativePath = "src/../upload/2.png";
        File file = new File(relativePath);
        FileInputStream fileInputStream = new FileInputStream(file);
        multipartFile = new MockMultipartFile("multipartFile", file.getName(), MediaType.IMAGE_PNG_VALUE, fileInputStream);
        // String imageUrl = "https://img.lovepik.com/original_origin_pic/18/10/31/a49241ca9d6280d96bb69a5f72fad7b8.png_wh860.png";
        // URL url = new URL(imageUrl);
        // byte[] imageData = url.openStream().readAllBytes();
        // InputStream inputStream = new ByteArrayInputStream(imageData);
        // multipartFile = new MockMultipartFile("multipartFile", "image.jpg", MediaType.IMAGE_JPEG_VALUE, inputStream);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void uploadSingleFile_AsAdmin_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/file-uploads")
                        .file(multipartFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void uploadSingleFile_AsRetailer_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/file-uploads")
                        .file(multipartFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void uploadSingleFile_AsCustomer_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/file-uploads")
                        .file(multipartFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithAnonymousUser
    void uploadSingleFile_AsAnonymousUser_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/file-uploads")
                        .file(multipartFile))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}
