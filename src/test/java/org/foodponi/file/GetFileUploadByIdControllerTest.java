package org.foodponi.file;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.file.enums.FileUploadErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class GetFileUploadByIdControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getFileUploadById_AsAdmin_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void getFileUploadById_AsRetailer_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void getFileUploadById_AsCustomer_ReturnsFileUpload() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithAnonymousUser
    void getFileUploadById_AsAnonymousUser_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getFileUploadById_WithFileUploadIdNotFound_AsAdmin_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(
                        FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND.getCode(),
                        FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND.getMessage(),
                        null
                )
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff100"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getFileUploadById_WithFileUploadFromAnotherUser_AsAdmin_ThrowsExceptiond() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(
                        CommonErrorCode.ACCESS_FORBIDDEN.getCode(),
                        CommonErrorCode.ACCESS_FORBIDDEN.getMessage(),
                        null
                )
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/file-uploads/{fileUploadId}", "ffffffff-ffff-ffff-ffff-ffffffffffff1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
