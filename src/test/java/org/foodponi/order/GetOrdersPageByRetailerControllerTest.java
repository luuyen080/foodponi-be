package org.foodponi.order;

import data.InitializationData;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class GetOrdersPageByRetailerControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4")
    void getOrdersPage_AsRetailer_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getOrdersPage_AsAdmin_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void getOrdersPage_AsCustomer_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void getOrdersPage_AsAnonymous_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4")
    void getOrdersPage_WithPageAndPageSize_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders")
                        .param("page", "0")
                        .param("pageSize", "10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(10));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4")
    void getOrdersPage_WithStatus_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders")
                        .param("status", "PENDING"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4")
    void getOrdersPage_WithPageAndPageSizeAndStatus_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/orders")
                        .param("page", "0")
                        .param("pageSize", "10")
                        .param("status", "PENDING"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(10));
    }

}
