package org.foodponi.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.order.enums.OrderErrorCode;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class GetOrderByIdByCustomerControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void getOrderById_AsCustomer_ReturnsOrder() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getOrderById_AsAdmin_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void getOrderById_AsRetailer_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void getOrderById_AsAnonymous_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void getOrderById_AsCustomer_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(OrderErrorCode.ORDER_NOT_FOUND.getCode(),
                        OrderErrorCode.ORDER_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo5"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(errorResponse)));
    }

}
