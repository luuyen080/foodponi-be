package org.foodponi.order;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.order.IOrderRepository;
import org.foodponi.business.order.OrderDTO;
import org.foodponi.business.order.enums.OrderErrorCode;
import org.foodponi.business.order_item.enums.OrderItemErrorCode;
import org.foodponi.business.product.enums.ProductErrorCode;
import org.foodponi.business.product_detail.IProductDetailRepository;
import org.foodponi.business.product_detail.enums.ProductDetailErrorCode;
import org.foodponi.business.user.IUserRepository;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.foodponi.core.model.UserPayload;
import org.foodponi.core.security.UserContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CreateOrderControllerTest {

    private @Autowired MockMvc mockMvc;
    private @Autowired IOrderRepository orderRepository;
    private @Autowired IProductDetailRepository productDetailRepository;
    private @Autowired IUserRepository userRepository;
    private @Autowired ObjectMapper objectMapper;
    private @Autowired JdbcTemplate jdbcTemplate;
    private InitializationData initializationData = new InitializationData();
    private @MockBean UserContext userContext;
    private OrderDTO.OrderRequestDTO orderRequestDTO;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);
        String jsonRequest = """
                {
                    "orderItems": [
                        {
                            "quantity": 5,
                            "productDetail": {
                                "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1"
                            },
                            "note": "Giam muoi"
                        },
                        {
                            "quantity": 2,
                            "productDetail": {
                                "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp2"
                            },
                            "note": "Giam duong"
                        }
                    ],
                    "shippingAddress": {
                        "fullName": "Hai Yen",
                        "phoneNumber": "0123456789",
                        "address": "789 Maple Avenue"
                    },
                    "note": "Giao som giup toi",
                    "payment": {
                        "method": "CASH",
                        "status": "PAYING"
                    }
                }""";
        orderRequestDTO = objectMapper.readValue(jsonRequest, OrderDTO.OrderRequestDTO.class);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_ValidOrder_AsCustomer_ReturnOrderResponseDTO() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu4")
    void create_ValidOrder_AsRetailer_ReturnOrderResponseDTO() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void create_ValidOrder_AsAdmin_ShouldThrowException() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.ACCESS_FORBIDDEN.getCode(),
                                CommonErrorCode.ACCESS_FORBIDDEN.getMessage(),
                                Collections.emptyMap())));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithAnonymousUser
    void create_ValidOrder_AsAnonymous_ShouldStatusForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithDuplicateProduct_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 5,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1"
                        },
                        "note": "Giam muoi"
                    },
                    {
                        "quantity": 2,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1"
                        },
                        "note": "Giam duong"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderItemErrorCode.ORDER_ITEM_DUPLICATED.getCode(),
                                OrderItemErrorCode.ORDER_ITEM_DUPLICATED.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithProductDetailNotFound_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 5,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp10"
                        },
                        "note": "Giam muoi"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(ProductDetailErrorCode.PRODUCT_DETAIL_NOT_FOUND.getCode(),
                                ProductDetailErrorCode.PRODUCT_DETAIL_NOT_FOUND.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithProductBlocked_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 5,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp5"
                        },
                        "note": "Giam muoi"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(ProductErrorCode.PRODUCT_BLOCKED.getCode(),
                                ProductErrorCode.PRODUCT_BLOCKED.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithProductDetailBlocked_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 5,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp4"
                        },
                        "note": "Giam muoi"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(ProductDetailErrorCode.PRODUCT_DETAIL_BLOCKED.getCode(),
                                ProductDetailErrorCode.PRODUCT_DETAIL_BLOCKED.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithDifferentRetailers_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 5,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1"
                        },
                        "note": "Giam muoi"
                    },
                    {
                        "quantity": 2,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp6"
                        },
                        "note": "Giam duong"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderItemErrorCode.ORDER_ITEM_NOT_SAME_RETAILER.getCode(),
                                OrderItemErrorCode.ORDER_ITEM_NOT_SAME_RETAILER.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void create_WithCustomerAndRetailerSame_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 5,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp3"
                        },
                        "note": "Giam muoi"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderErrorCode.ORDER_CUSTOMER_RETAILER_SAME.getCode(),
                                OrderErrorCode.ORDER_CUSTOMER_RETAILER_SAME.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithOrderItemsContainsNullProductDetail_ShouldThrowException() throws Exception {
        orderRequestDTO.setOrderItems(objectMapper.readValue("[null]", new TypeReference<>() {
        }));

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderItemErrorCode.ORDER_ITEM_CONTAINS_NULL_OBJECT.getCode(),
                                OrderItemErrorCode.ORDER_ITEM_CONTAINS_NULL_OBJECT.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithOrderItemsIsNull_ShouldThrowException() throws Exception {
        orderRequestDTO.setOrderItems(null);

        Map<String, Object> details = new HashMap<>();
        details.put("orderItems", "must not be empty");

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                                CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                                details)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithOrderItemsIsEmpty_ShouldThrowException() throws Exception {
        orderRequestDTO.setOrderItems(new ArrayList<>());

        Map<String, Object> details = new HashMap<>();
        details.put("orderItems", "must not be empty");

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                                CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                                details)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithQuantitySmallerThan1_ShouldThrowException() throws Exception {
        String jsonOrderItems = """
                [
                    {
                        "quantity": 0,
                        "productDetail": {
                            "id": "pdpdpdpd-pdpd-pdpd-pdpd-pdpdpdpdpdp1"
                        },
                        "note": "Giam muoi"
                    }
                ]""";

        orderRequestDTO.setOrderItems(objectMapper.readValue(jsonOrderItems, new TypeReference<>() {
        }));

        Map<String, Object> details = new HashMap<>();
        details.put("orderItems[0].quantity", "must be greater than or equal to 1");

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                                CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                                details)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithShippingAddressIsNull_ShouldThrowException() throws Exception {
        orderRequestDTO.setShippingAddress(null);

        Map<String, Object> details = new HashMap<>();
        details.put("shippingAddress", "must not be null");

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                                CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                                details)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void create_WithPaymentIsNull_ShouldThrowException() throws Exception {
        orderRequestDTO.setPayment(null);

        Map<String, Object> details = new HashMap<>();
        details.put("payment", "must not be null");

        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                                CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                                details)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/orders")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(orderRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

}
