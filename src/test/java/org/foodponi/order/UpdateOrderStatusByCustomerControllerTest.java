package org.foodponi.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.order.enums.OrderErrorCode;
import org.foodponi.business.order.enums.OrderStatus;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UpdateOrderStatusByCustomerControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateOrderStatus_ValidRequest_ReturnsOrder() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1")
                        .param("orderStatus", "CANCELLED"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithAnonymousUser
    void updateOrderStatus_AsAnonymous_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1")
                        .param("orderStatus", "CANCELLED"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateOrderStatus_AsAdmin_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1")
                        .param("orderStatus", "CANCELLED"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "RETAILER")
    void updateOrderStatus_AsRetailer_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1")
                        .param("orderStatus", "CANCELLED"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateOrderStatus_InvalidOrderId_ThrowsForbidden() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderErrorCode.ORDER_NOT_FOUND.getCode(),
                                OrderErrorCode.ORDER_NOT_FOUND.getMessage(),
                                null)));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/customer/orders/{id}", "invalidId")
                        .param("orderStatus", "CANCELLED"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "invalidUser")
    void updateOrderStatus_InvalidUser_ThrowsForbidden() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderErrorCode.ORDER_NOT_FOUND.getCode(),
                                OrderErrorCode.ORDER_NOT_FOUND.getMessage(),
                                null)));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/customer/orders/{id}", "oooooooo-oooo-oooo-oooo-ooooooooooo1")
                        .param("orderStatus", "CANCELLED"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string(jsonBody));
    }

}

