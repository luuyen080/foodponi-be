package org.foodponi.order_item;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.order.enums.OrderErrorCode;
import org.foodponi.business.order_item.Rate;
import org.foodponi.business.order_item.enums.OrderItemErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CreateRateOrderItemControllerTest {

    private @Autowired MockMvc mockMvc;
    private @Autowired ObjectMapper objectMapper;
    private @Autowired JdbcTemplate jdbcTemplate;
    private InitializationData initializationData = new InitializationData();
    private Rate rate;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);

        rate = Rate.builder()
                .rate(4)
                .message("message")
                .images(null)
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_ReturnsOrderItem() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_WithMessageEmpty_ReturnsOrderItem() throws Exception {
        rate.setMessage("");
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_WithMessageNull_ReturnsOrderItem() throws Exception {
        rate.setMessage(null);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_WithImagesNull_ReturnsOrderItem() throws Exception {
        rate.setImages(null);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_WithImagesEmpty_ReturnsOrderItem() throws Exception {
        rate.setImages(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_WithRateNull_ReturnsOrderItem() throws Exception {
        rate.setRate(null);

        Map<String, Object> details = new HashMap<>();
        details.put("rate", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
            new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                    CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details)
        );
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createRate_AsAdmin_ThrowsForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void createRate_AsAnonymous_ThrowsForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createRate_AsCustomer_UserNotInOrder_ThrowsException() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(CommonErrorCode.ACCESS_FORBIDDEN.getCode(),
                                CommonErrorCode.ACCESS_FORBIDDEN.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_OrderStatusNotCompleted_ThrowsException() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderErrorCode.ORDER_MUST_BE_COMPLETED.getCode(),
                                OrderErrorCode.ORDER_MUST_BE_COMPLETED.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createRate_AsCustomer_OrderItemRated_ThrowsException() throws Exception {
        String jsonBody = objectMapper.writeValueAsString(
                new ErrorResponse(
                        new ErrorResponse.ErrorInfo(OrderItemErrorCode.ORDER_ITEM_ALREADY_RATED.getCode(),
                                OrderItemErrorCode.ORDER_ITEM_ALREADY_RATED.getMessage(),
                                null)));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/order-items/rate/{orderItemId}", "oioioioi-oioi-oioi-oioi-oioioioioio5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(rate)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string(jsonBody));
    }

}
