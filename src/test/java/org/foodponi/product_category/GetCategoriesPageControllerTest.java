package org.foodponi.product_category;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.product_category.CategoryDTO;
import org.foodponi.business.product_category.ICategoryRepository;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.foodponi.core.util.MapperUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class GetCategoriesPageControllerTest {

    private @Autowired MockMvc mockMvc;
    private @Autowired ICategoryRepository categoryRepository;
    private @Autowired ObjectMapper objectMapper;
    private @Autowired JdbcTemplate jdbcTemplate;
    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithAnonymousUser
    void getOnlyParentCategoriesPage_AsAnonymous_ReturnsPage() throws Exception {
        // Arrange
        boolean onlyParent = true;

        //Expected
        Page<CategoryDTO.CategoryResponseDTO> categoryResponseDTOPage
                = MapperUtils.mapPage(categoryRepository.findAllByParentCategoryIsNull(Pageable.unpaged()), CategoryDTO.CategoryResponseDTO.class);

        String jsonBody = objectMapper.writeValueAsString(categoryResponseDTOPage);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/product-categories")
                        .param("onlyParent", String.valueOf(onlyParent)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray());
//                .andExpect(MockMvcResultMatchers.content().string(jsonBody));
    }

    @Test
    @WithAnonymousUser
    void getCategories_WithNegativePage_ReturnsError() throws Exception {
        // Arrange
        Integer page = -1;
        Integer pageSize = 0;

        //Expected
        Map<String, Object> details = Map.of(
                "Min.page", "must be greater than or equal to 0",
                "Min.pageSize", "must be greater than or equal to 1"
        );
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details)
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/product-categories")
                        .param("page", String.valueOf(page))
                        .param("pageSize", String.valueOf(pageSize)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
