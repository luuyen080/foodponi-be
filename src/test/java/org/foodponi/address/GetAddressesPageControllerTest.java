package org.foodponi.address;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.product_category.ICategoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class GetAddressesPageControllerTest {

    private @Autowired MockMvc mockMvc;
    private @Autowired ICategoryRepository categoryRepository;
    private @Autowired ObjectMapper objectMapper;
    private @Autowired JdbcTemplate jdbcTemplate;
    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getAddressesPage_AsAdmin_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void getAddressesPage_AsCustomer_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void getAddressesPage_AsRetailer_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getAddressesPage_WithPage_AsAdmin_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses")
                        .param("page", "0"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(3));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getAddressesPage_WithPageSize_AsAdmin_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses")
                        .param("pageSize", "10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(3));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getAddressesPage_WithPageAndPageSize_AsAdmin_ReturnsPage() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses")
                        .param("page", "0")
                        .param("pageSize", "10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(10));
    }

    @Test
    @WithAnonymousUser
    void getAddressesPage_AsAnonymousUser_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}
