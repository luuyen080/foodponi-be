package org.foodponi.address;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.address.enums.AddressErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DeleteAddressByIdControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteAddressById_AsAdmin_ReturnNoContent() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/addresses/{addressId}", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa7"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void deleteAddressById_AsCustomer_ReturnNoContent() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/addresses/{addressId}", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa5"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void deleteAddressById_AsRetailer_ReturnNoContent() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/addresses/{addressId}", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa6"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithAnonymousUser
    void deleteAddressById_AsAnonymousUser_ThrowsForbidden() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/addresses/{addressId}", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteAddressById_WithAddressNotFound_AsAdmin_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(AddressErrorCode.ADDRESS_NOT_FOUND.getCode(),
                        AddressErrorCode.ADDRESS_NOT_FOUND.getMessage(),
                        null)
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/addresses/{addressId}", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa9"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteAddressById_WithAddressFromAnotherUser_AsAdmin_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.ACCESS_FORBIDDEN.getCode(),
                        CommonErrorCode.ACCESS_FORBIDDEN.getMessage(),
                        null)
        );

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/addresses/{addressId}", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaa5"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
