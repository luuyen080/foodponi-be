package org.foodponi.address;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.address.Address;
import org.foodponi.business.address.AddressDTO;
import org.foodponi.business.address.enums.AddressErrorCode;
import org.foodponi.business.auth.Auth;
import org.foodponi.business.product_category.ICategoryRepository;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CreateAddressControllerTest {

    private @Autowired MockMvc mockMvc;
    private @Autowired ICategoryRepository categoryRepository;
    private @Autowired ObjectMapper objectMapper;
    private @Autowired JdbcTemplate jdbcTemplate;
    private InitializationData initializationData = new InitializationData();

    private AddressDTO.AddressRequestDTO addressRequestDTO;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
        addressRequestDTO = AddressDTO.AddressRequestDTO.builder()
                .fullName("fullName")
                .phoneNumber("phoneNumber")
                .address("address")
                .lon(0.0)
                .lat(0.0)
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1") void createAddress_ValidRequest_ReturnsAddress() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_AddressExist_ThrowsException() throws Exception {
        addressRequestDTO.setFullName("Emily Johnson");
        addressRequestDTO.setPhoneNumber("5555555555");
        addressRequestDTO.setAddress("GHI Street, 456 Ward");
        addressRequestDTO.setLon(105.72105620969991);
        addressRequestDTO.setLat(21.04874565);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(AddressErrorCode.ADDRESS_EXISTED.getCode(),
                        AddressErrorCode.ADDRESS_EXISTED.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithFullNameNull__ThrowsException() throws Exception {
        addressRequestDTO.setFullName(null);

        Map<String, Object> details = new HashMap<>();
        details.put("fullName", "must not be blank");
//        details.put("fullName", "Fullname cannot be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithFullNameEmpty__ThrowsException() throws Exception {
        addressRequestDTO.setFullName("");

        Map<String, Object> details = new HashMap<>();
        details.put("fullName", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithPhoneNumberEmpty__ThrowsException() throws Exception {
        addressRequestDTO.setPhoneNumber("");

        Map<String, Object> details = new HashMap<>();
        details.put("phoneNumber", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithPhoneNumberNull__ThrowsException() throws Exception {
        addressRequestDTO.setPhoneNumber(null);

        Map<String, Object> details = new HashMap<>();
        details.put("phoneNumber", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithFullNameNullAndPhoneNumberNullAndAddressNull_ThrowsException() throws Exception {
        addressRequestDTO.setFullName(null);
        addressRequestDTO.setPhoneNumber(null);
        addressRequestDTO.setAddress(null);

        Map<String, Object> details = new HashMap<>();
        details.put("fullName", "must not be blank");
        details.put("phoneNumber", "must not be blank");
        details.put("address", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithFullNameEmptyAndPhoneNumberEmptyAndAddressEmpty_ThrowsException() throws Exception {
        addressRequestDTO.setFullName("");
        addressRequestDTO.setPhoneNumber("");
        addressRequestDTO.setAddress("");

        Map<String, Object> details = new HashMap<>();
        details.put("fullName", "must not be blank");
        details.put("phoneNumber", "must not be blank");
        details.put("address", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithAddressNull__ThrowsException() throws Exception {
        addressRequestDTO.setAddress(null);

        Map<String, Object> details = new HashMap<>();
        details.put("address", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createAddress_WithAddressEmpty_ThrowsException() throws Exception {
        addressRequestDTO.setAddress("");

        Map<String, Object> details = new HashMap<>();
        details.put("address", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void createAddress_AsAnonymous_ThrowsException() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}
