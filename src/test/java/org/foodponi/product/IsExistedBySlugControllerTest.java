package org.foodponi.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.product.ProductDTO;
import org.foodponi.business.product_detail.ProductDetailDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.Assert;

import java.util.List;
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class IsExistedBySlugControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    private ProductDTO.ProductCreateRequestDTO productCreateRequestDTO;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void isExistedBySlug_AsAdmin_ReturnsTrue() throws Exception {
        // Arrange
        String slug = "p-1";

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/is-existed-by-slug")
                        .param("slug", slug))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void isExistedBySlug_AsRetailer_ReturnsTrue() throws Exception {
        // Arrange
        String slug = "p-1";

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/is-existed-by-slug")
                        .param("slug", slug))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void isExistedBySlug_AsCustomer_ReturnsTrue() throws Exception {
        // Arrange
        String slug = "p-1";

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/is-existed-by-slug")
                        .param("slug", slug))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }

    @Test
    @WithAnonymousUser
    void isExistedBySlug_AsAnonymousUser_ReturnsTrue() throws Exception {
        // Arrange
        String slug = "p-1";

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/is-existed-by-slug")
                        .param("slug", slug))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void isExistedBySlug_WithSlugNull_AsCustomer_ThrowsException() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/is-existed-by-slug"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void isExistedBySlug_WithSlugNotFound_AsAdmin_ReturnsFalse() throws Exception {
        // Arrange
        String slug = "non-existing-slug";

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/is-existed-by-slug")
                        .param("slug", slug))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("false"));
    }

}
