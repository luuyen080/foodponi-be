package org.foodponi.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.product.enums.ProductErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class GetProductByIdAndRetailerControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void getProductByIdAndRetailer_AsRetailer_ReturnProduct() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void getProductByIdAndRetailer_AsAdmin_ThrowsException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void getProductByIdAndRetailer_AsCustomer_ThrowsException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void getProductByIdAndRetailer_AsAnonymous_ThrowsException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void getProductByIdAndRetailer_AsRetailer_ReturnProductNotFound() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(ProductErrorCode.PRODUCT_NOT_FOUND.getCode(),
                        ProductErrorCode.PRODUCT_NOT_FOUND.getMessage(),
                        null)
        );

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/retailer/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp6"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}