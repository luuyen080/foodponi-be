package org.foodponi.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.product.ProductDTO;
import org.foodponi.business.product.enums.ProductErrorCode;
import org.foodponi.business.product_detail.ProductDetailDTO;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CreateProductControllerTest {
    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    private ProductDTO.ProductCreateRequestDTO productCreateRequestDTO;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);
        productCreateRequestDTO = ProductDTO.ProductCreateRequestDTO.builder()
                .name("name")
                .slug("slug")
                .thumbnail("thumbnail")
                .shortDescription("shortDescription")
                .status(true)
                .productDetails(List.of(ProductDetailDTO.ProductDetailCreateRequestDTO.builder().build()))
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createProduct_AsRetailer_ValidRequest_ReturnsProduct() throws Exception {
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void createProduct_AsAdmin_ValidRequest_ThrowsException() throws Exception {
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void createProduct_AsCustomer_ValidRequest_ThrowsException() throws Exception {
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void createProduct_AsAnonymousUser_ValidRequest_ThrowsException() throws Exception {
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createProduct_WithSlugExisted_AsRetailer_ThrowsException() throws Exception {
        productCreateRequestDTO.setName("P1");
        productCreateRequestDTO.setThumbnail("img.jpeg");
        productCreateRequestDTO.setShortDescription("sdesc");
        productCreateRequestDTO.setSlug("p-1");
        productCreateRequestDTO.setProductDetails(List.of(ProductDetailDTO.ProductDetailCreateRequestDTO.builder().build()));
        productCreateRequestDTO.setStatus(true);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(ProductErrorCode.PRODUCT_SLUG_EXISTED.getCode(),
                        ProductErrorCode.PRODUCT_SLUG_EXISTED.getMessage(), null));
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productCreateRequestDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createCategory_WithCategoryNameNull_AsRetailer_ReturnsError() throws Exception {
        productCreateRequestDTO.setName(null);
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("name", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createCategory_WithCategoryNameEmpty_AsRetailer_ReturnsError() throws Exception {
        productCreateRequestDTO.setName("");
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("name", "must not be blank");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createCategory_WithProductDetailNull_AsRetailer_ReturnsError() throws Exception {
        productCreateRequestDTO.setProductDetails(null);
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("productDetails", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createCategory_WithCategoryNameNullAndProductDetailNull_AsRetailer_ReturnsError() throws Exception {
        productCreateRequestDTO.setName(null);
        productCreateRequestDTO.setProductDetails(null);
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("name", "must not be blank");
        details.put("productDetails", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void createCategory_WithPriceSmallerThan0_AsRetailer_ReturnsError() throws Exception {
        productCreateRequestDTO.setProductDetails(
                List.of(ProductDetailDTO.ProductDetailCreateRequestDTO.builder().price(-1).build()));
        String requestBody = objectMapper.writeValueAsString(productCreateRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("productDetails[0].price","must be greater than or equal to 0");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details));
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
