package org.foodponi.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.product.enums.ProductErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DeleteProductByAdminControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired ObjectMapper objectMapper;

    private @Autowired JdbcTemplate jdbcTemplate;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() throws JsonProcessingException {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteProductByAdmin_AsAdmin_ReturnNoContent() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void deleteProductByAdmin_AsRetailer_ThrowsForbbiden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void deleteProductByAdmin_AsCustomer_ThrowsForbbiden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void deleteProductByAdmin_AsAnonymousUser_ThrowsForbbiden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/products/{productId}", "ppppppp-pppp-pppp-pppp-ppppppppppp1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteProductByAdmin_WithProductNotFound_AsAdmin_ThrowsException() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(ProductErrorCode.PRODUCT_NOT_FOUND.getCode(), ProductErrorCode.PRODUCT_NOT_FOUND.getMessage(), null)
        );

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/products/{productId}", "1000"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
