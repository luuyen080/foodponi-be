package org.foodponi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.address.AddressDTO;
import org.foodponi.business.address.enums.AddressErrorCode;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UpdateUserAddressControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    private AddressDTO.AddressIdDTO addressIdDTO;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);

        addressIdDTO = AddressDTO.AddressIdDTO.builder()
                .id("3fc53a4a-ba7e-45b3-89d3-c7e887c6e38e")
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserAddress_ValidRequest_AsAdmin_ReturnsUser() throws Exception {
        addressIdDTO.setId("d54f5b31-1764-451e-a1b9-4dd85a1fe5eb");
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void updateUserAddress_ValidRequest_AsRetailer_ReturnsUser() throws Exception {
        addressIdDTO.setId("a3701501-1d80-41d2-85f2-512bb6da83eb");
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserAddress_ValidRequest_AsCustomer_ReturnsUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithAnonymousUser
    void updateUserAddress_ValidRequest_AsAnonymous_ThrowsForbidden() throws Exception {
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu6")
    void updateUserAddress_WithUserIdNotFound_AsCustomer_ThrowsException() throws Exception {
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USER_NOT_FOUND.getCode(),
                        UserErrorCode.USER_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserAddress_WithAddressNotFound_AsCustomer_ThrowsException() throws Exception {
        addressIdDTO.setId("d54f5b31-1764-451e-a1b9-4dd85a1fe5ec");
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(AddressErrorCode.ADDRESS_NOT_FOUND.getCode(),
                        AddressErrorCode.ADDRESS_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserAddress_WithAddressFromAnotherUserId_AsCustomer_ThrowsException() throws Exception {
        addressIdDTO.setId("d54f5b31-1764-451e-a1b9-4dd85a1fe5eb");
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(AddressErrorCode.ADDRESS_NOT_FOUND.getCode(),
                        AddressErrorCode.ADDRESS_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserAddress_WithAddressIsNull_AsCustomer_ThrowsException() throws Exception {
        addressIdDTO.setId(null);
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("id", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserAddress_WithAddressIsEmpty_AsCustomer_ThrowsException() throws Exception {
        addressIdDTO.setId("");
        String requestBody = objectMapper.writeValueAsString(addressIdDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("id", "size must be between 36 and 36");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
