package org.foodponi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class DeleteUserByAdminControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteUserByAdmin_AsAdmin_ReturnNoContent() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteUserByAdmin_ByAnotherAdmin_AsAdmin_ReturnNoContent() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "87d87793-fa2c-44cf-8fe1-065d240458b5"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteUserByAdmin_ByItself_AsAdmin_ReturnForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void deleteUserByAdmin_ByRetailer_ReturnForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void deleteUserByAdmin_ByCustomer_ReturnForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void deleteUserByAdmin_AsAnonymousUser_ReturnForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    //chỉ có admin mới có thể vào trường xóa với uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu không tồn tại
    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void deleteUserByAdmin_WithIdNotFound_ByAdmin_ReturnBadRequest() throws Exception {
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USER_NOT_FOUND.getCode(),
                        UserErrorCode.USER_NOT_FOUND.getMessage(),
                        null)
        );

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/admin/users/{id}", "1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
