package org.foodponi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.address.AddressDTO;
import org.foodponi.business.address.enums.AddressErrorCode;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ChangePasswordControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    private UserDTO.ChangePasswordRequestDTO changePasswordRequestDTO;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);

        changePasswordRequestDTO = UserDTO.ChangePasswordRequestDTO.builder()
                .oldPassword("123456")
                .newPassword("newPassword")
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void changePassword_ValidRequest_AsAdmin() throws Exception {
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void changePassword_ValidRequest_AsRetailer() throws Exception {
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_ValidRequest_AsCustomer() throws Exception {
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithAnonymousUser
    void changePassword_AsAnonymous_ThrowsForbidden() throws Exception {
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu6")
    void changePassword_WithUserNotFound_AsCustomer() throws Exception {
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USER_NOT_FOUND.getCode(),
                        UserErrorCode.USER_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordNotMatch_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("wrongPassword");
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.CURRENT_PASSWORD_NOT_MATCH.getCode(),
                        UserErrorCode.CURRENT_PASSWORD_NOT_MATCH.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordIsNull_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword(null);
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithNewPasswordIsNull_AsCustomer() throws Exception {
        changePasswordRequestDTO.setNewPassword(null);
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("newPassword", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordAndNewPasswordAreNull_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword(null);
        changePasswordRequestDTO.setNewPassword(null);
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "must not be null");
        details.put("newPassword", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordSmallerThan6Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("a".repeat(5));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordGreaterThan50Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("a".repeat(51));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithNewPasswordSmallerThan6Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setNewPassword("a".repeat(5));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("newPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithNewPasswordGreaterThan50Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setNewPassword("a".repeat(51));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("newPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordAndNewPasswordSmallerThan6Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("a".repeat(5));
        changePasswordRequestDTO.setNewPassword("a".repeat(5));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "length must be between 6 and 50");
        details.put("newPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordAndNewPasswordGreaterThan50Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("a".repeat(51));
        changePasswordRequestDTO.setNewPassword("a".repeat(51));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "length must be between 6 and 50");
        details.put("newPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordSmallerThan6CharactersAndNewPasswordGreaterThan50Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("a".repeat(5));
        changePasswordRequestDTO.setNewPassword("a".repeat(51));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "length must be between 6 and 50");
        details.put("newPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void changePassword_WithOldPasswordGreaterThan50CharactersAndNewPasswordSmallerThan6Characters_AsCustomer() throws Exception {
        changePasswordRequestDTO.setOldPassword("a".repeat(51));
        changePasswordRequestDTO.setNewPassword("a".repeat(5));
        String requestBody = objectMapper.writeValueAsString(changePasswordRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("oldPassword", "length must be between 6 and 50");
        details.put("newPassword", "length must be between 6 and 50");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
