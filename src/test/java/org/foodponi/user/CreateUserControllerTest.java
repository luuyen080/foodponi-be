package org.foodponi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CreateUserControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    private UserDTO.UserCreationRequestDTO userCreationRequestDTO;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);

        userCreationRequestDTO = UserDTO.UserCreationRequestDTO.builder()
                .username("username")
                .email("email@gmail.com")
                .password("password").build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithAnonymousUser
    void createUser_ValidRequest_ReturnsUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userCreationRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithAnonymousUser
    void createUser_InvalidEmail_ThrowsException() throws Exception {
        userCreationRequestDTO.setEmail("invalid_email");
        String requestBody = objectMapper.writeValueAsString(userCreationRequestDTO);

        Map<String, Object> details = Map.of("email", "must be a well-formed email address");
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void createUser_UsernameAndEmailNull_ThrowsException() throws Exception {
        userCreationRequestDTO.setUsername(null);
        userCreationRequestDTO.setEmail(null);
        String requestBody = objectMapper.writeValueAsString(userCreationRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.EMAIL_AND_USERNAME_MUST_NOT_BE_NULL.getCode(),
                        UserErrorCode.EMAIL_AND_USERNAME_MUST_NOT_BE_NULL.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void createUser_UsernameExist_ThrowsException() throws Exception {
        userCreationRequestDTO.setUsername("customer1");
        String requestBody = objectMapper.writeValueAsString(userCreationRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USERNAME_EXIST.getCode(),
                        UserErrorCode.USERNAME_EXIST.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void createUser_EmailExist_ThrowsException() throws Exception {
        userCreationRequestDTO.setEmail("customer1@gmail.com");
        String requestBody = objectMapper.writeValueAsString(userCreationRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.EMAIL_EXIST.getCode(),
                        UserErrorCode.EMAIL_EXIST.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
