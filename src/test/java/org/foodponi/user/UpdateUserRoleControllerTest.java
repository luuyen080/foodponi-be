package org.foodponi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.Role;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UpdateUserRoleControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    private UserDTO.UserUpdateRoleRequestDTO userUpdateRoleRequestDTO;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);

        userUpdateRoleRequestDTO = UserDTO.UserUpdateRoleRequestDTO.builder()
                .id("6d0bb5ac-3c1f-4092-96fe-5d27bfc50dc9")
                .role(Role.ADMIN)
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_ValidRequest_AsAdmin_ReturnsUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void updateUserRole_ValidRequest_AsRetailer_ThrowsForbidden() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserRole_ValidRequest_AsCusTomer_ThrowsForbidden() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void updateUserRole_ValidRequest_AsAnonymous_ThrowsForbidden() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "87d87793-fa2c-44cf-8fe1-065d240458b5")
    void updateUserRole_ByItself_AsAdmin_ThrowsForbidden() throws Exception {
        userUpdateRoleRequestDTO.setId("87d87793-fa2c-44cf-8fe1-065d240458b5");
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.ACCESS_FORBIDDEN.getCode(),
                        CommonErrorCode.ACCESS_FORBIDDEN.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_WithUserNotFound_ThrowsException() throws Exception {
        userUpdateRoleRequestDTO.setId("6d0bb5ac-3c1f-4092-96fe-5d27bfc50dc7");
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USER_NOT_FOUND.getCode(),
                        UserErrorCode.USER_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_WithIdIsNull_ThrowsException() throws Exception {
        userUpdateRoleRequestDTO.setId(null);
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("id", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_WithIdIsEmpty_ThrowsException() throws Exception {
        userUpdateRoleRequestDTO.setId("");
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("id", "size must be between 36 and 36");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_WithIdSmallerThan36Characters_ThrowsException() throws Exception {
        userUpdateRoleRequestDTO.setId("aaa");
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("id", "size must be between 36 and 36");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_WithIdGreaterThan36Characters_ThrowsException() throws Exception {
        userUpdateRoleRequestDTO.setId("a".repeat(37));
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("id", "size must be between 36 and 36");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserRole_WithRoleIsNull_ThrowsException() throws Exception {
        userUpdateRoleRequestDTO.setRole(null);
        String requestBody = objectMapper.writeValueAsString(userUpdateRoleRequestDTO);

        Map<String, Object> details = new HashMap<>();
        details.put("role", "must not be null");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/admin/users/update-role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
