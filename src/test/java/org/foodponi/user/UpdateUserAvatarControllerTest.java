package org.foodponi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UpdateUserAvatarControllerTest {

    private @Autowired MockMvc mockMvc;

    private @Autowired JdbcTemplate jdbcTemplate;

    private @Autowired ObjectMapper objectMapper;

    private InitializationData initializationData = new InitializationData();

    private UserDTO.UserUpdateAvatarRequestDTO userUpdateAvatarRequestDTO;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);

        userUpdateAvatarRequestDTO = UserDTO.UserUpdateAvatarRequestDTO.builder()
                .avatar("avatar")
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithMockUser(roles = "ADMIN", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu3")
    void updateUserInfo_ValidRequest_AsAdmin_ReturnsUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateAvatarRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-avatar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "RETAILER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu2")
    void updateUserInfo_ValidRequest_AsRetailer_ReturnsUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateAvatarRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-avatar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserInfo_ValidRequest_AsCustomer_ReturnsUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateAvatarRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-avatar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserInfo_WithAvatarIsNull_AsCustomer_ReturnsUser() throws Exception {
        userUpdateAvatarRequestDTO.setAvatar(null);
        String requestBody = objectMapper.writeValueAsString(userUpdateAvatarRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-avatar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithMockUser(roles = "CUSTOMER", username = "uuuuuuuu-uuuu-uuuu-uuuu-uuuuuuuuuuu1")
    void updateUserInfo_WithAvatarIsEmpty_AsCustomer_ReturnsUser() throws Exception {
        userUpdateAvatarRequestDTO.setAvatar("");
        String requestBody = objectMapper.writeValueAsString(userUpdateAvatarRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-avatar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    @WithAnonymousUser
    void updateUserInfo_ValidRequest_AsAnonymous_ThrowsForbidden() throws Exception {
        String requestBody = objectMapper.writeValueAsString(userUpdateAvatarRequestDTO);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/users/update-avatar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}
