package org.foodponi.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.InitializationData;
import org.foodponi.business.auth.Auth;
import org.foodponi.business.product_category.ICategoryRepository;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.model.ErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LoginControllerTest {

    private @Autowired MockMvc mockMvc;
    private @Autowired ObjectMapper objectMapper;
    private @Autowired JdbcTemplate jdbcTemplate;
    private InitializationData initializationData = new InitializationData();

    private Auth.AuthenticationRequest authenticationRequest;

    @BeforeEach
    void setUp() {
        initializationData.insertAll(jdbcTemplate);
        authenticationRequest = Auth.AuthenticationRequest.builder()
                .email("customer1@gmail.com")
                .password("123456")
                .build();
    }

    @AfterEach
    void tearDown() {
        initializationData.deleteAll(jdbcTemplate);
    }

    @Test
    @WithAnonymousUser
    void login_ValidEmailRequest_ReturnsToken() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.accessToken").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.refreshToken").isString());
    }

    @Test
    @WithAnonymousUser
    void login_ValidUsernameRequest_ReturnsToken() throws Exception {
        authenticationRequest.setEmail(null);
        authenticationRequest.setUsername("customer1");

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.accessToken").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.refreshToken").isString());
    }

    @Test
    @WithAnonymousUser
    void login_InvalidEmailRequest_ThrowsException() throws Exception {
        authenticationRequest.setEmail("invalidEmail");

        Map<String, Object> details = Map.of("email", "must be a well-formed email address");
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void login_WrongEmailRequest_ThrowsException() throws Exception {
        authenticationRequest.setEmail("wrongEmail@gmail.com");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USER_NOT_FOUND.getCode(),
                        UserErrorCode.USER_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void login_InvalidUsernameRequest_ThrowsException() throws Exception {
        authenticationRequest.setEmail(null);
        authenticationRequest.setUsername("cust");

        Map<String, Object> details = Map.of("username", "length must be between 5 and 50");
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void login_WrongUsernameRequest_ThrowsException() throws Exception {
        authenticationRequest.setEmail(null);
        authenticationRequest.setUsername("wrongUsername");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.USER_NOT_FOUND.getCode(),
                        UserErrorCode.USER_NOT_FOUND.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void login_InvalidPasswordRequest_ThrowsException() throws Exception {
        authenticationRequest.setPassword("w");

        Map<String, Object> details = Map.of("password", "length must be between 6 and 50");
        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                        CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(),
                        details));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

    @Test
    @WithAnonymousUser
    void login_WrongPasswordRequest_ThrowsException() throws Exception {
        authenticationRequest.setPassword("wrongPassword");

        ErrorResponse errorResponse = new ErrorResponse(
                new ErrorResponse.ErrorInfo(UserErrorCode.CURRENT_PASSWORD_NOT_MATCH.getCode(),
                        UserErrorCode.CURRENT_PASSWORD_NOT_MATCH.getMessage(),
                        null));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequest)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(errorResponse)));
    }

}
