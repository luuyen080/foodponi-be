package org.foodponi.core.enums;

import org.foodponi.business.user.enums.UserPrivilege;

import java.util.Set;

public enum Module {

    USER(1, UserPrivilege.FIND);

    Module(int value, IPrivilege... privileges) {
        this.value = value;
        this.privileges = privileges;
    }

    final int value;
    final IPrivilege[] privileges;

    public int value() {
        return value;
    }

    public Set<IPrivilege> privileges() {
        return Set.of(privileges);
    }

}
