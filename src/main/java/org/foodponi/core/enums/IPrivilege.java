package org.foodponi.core.enums;

public interface IPrivilege {

    int value();

    String name();

}
