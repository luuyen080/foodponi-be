package org.foodponi.core.enums;

public enum CommonErrorCode implements IErrorCode {

    UNKNOWN("CM00", "Unknown"),
    ACCESS_TOKEN_INVALID("CM02", "Access token invalid"),
    ACCESS_FORBIDDEN("CM03", "Access Forbidden"),
    PRINCIPAL_INVALID("CM04", "Principal invalid"),
    REQUEST_DATA_VALIDATION_FAILURE("CM05", "Request validation failure");

    CommonErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
