package org.foodponi.core.enums;

public interface IErrorCode {

    String getCode();

    String getMessage();

}
