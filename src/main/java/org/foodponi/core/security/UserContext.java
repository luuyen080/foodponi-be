package org.foodponi.core.security;

import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.UserPayload;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class UserContext {

    public UserPayload get() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth instanceof SimpleAuthenticationToken simpleAuthentication) {
            if (simpleAuthentication.getPrincipal() instanceof UserContextDetails userContextDetails) {
                return userContextDetails.getUser();
            }

            throw new FoodException(HttpStatus.INTERNAL_SERVER_ERROR, CommonErrorCode.PRINCIPAL_INVALID, "Principal authentication not instance of " + UserContextDetails.class.getSimpleName());
        }

        throw new FoodException(HttpStatus.FORBIDDEN, CommonErrorCode.ACCESS_FORBIDDEN, CommonErrorCode.ACCESS_FORBIDDEN.getMessage());
    }

}
