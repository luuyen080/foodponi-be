package org.foodponi.core.security;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.user.IUserRepository;
import org.foodponi.business.user.User;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.ErrorResponse;
import org.foodponi.core.model.UserPayload;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@Slf4j
public class JWTAuthenticationFilter extends OncePerRequestFilter {

    private ObjectMapper objectMapper;

    private final JWTService jwtService;

    private final IUserRepository userRepository;

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        try {
            String token = jwtService.resolveToken(request);

            if (!ObjectUtils.isEmpty(token)) {
                DecodedJWT decodedJWT = jwtService.verifyAccessToken(token);
                UserPayload userPayload = objectMapper.readValue(Base64.getDecoder().decode(decodedJWT.getPayload().getBytes()), UserPayload.class);

                User user = userRepository.findById(userPayload.getId()).orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));
                if (!user.isStatus())
                    throw new FoodException(UserErrorCode.USER_BLOCKED);

                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                        new org.springframework.security.core.userdetails.User(
                                user.getId(),
                                "",
                                user.getRole().getAuthorities()
                        ),
                        null,
                        user.getRole().getAuthorities()
                ));
            }
        } catch (JWTVerificationException e) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpStatus.UNAUTHORIZED.value());

            ErrorResponse errorResponse = new ErrorResponse(CommonErrorCode.ACCESS_TOKEN_INVALID.getCode(), CommonErrorCode.ACCESS_TOKEN_INVALID.getMessage(), Map.of());

            response.getWriter().append(objectMapper.writeValueAsString(errorResponse));

            return;
        } catch (JsonProcessingException e) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());

            ErrorResponse errorResponse = new ErrorResponse(CommonErrorCode.UNKNOWN.getCode(), "unexpected parse user payload request", Map.of());

            response.getWriter().append(objectMapper.writeValueAsString(errorResponse));

            return;
        } catch (FoodException e) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(e.getHttpStatus().value());

            ErrorResponse errorResponse = new ErrorResponse(e.getErrorCode(), e.getErrorMessage(), Map.of());

            response.getWriter().append(objectMapper.writeValueAsString(errorResponse));

            return;
        }

        filterChain.doFilter(request, response);
    }

}
