package org.foodponi.core.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.UserPayload;
import org.foodponi.core.util.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

import static java.time.ZoneId.systemDefault;

@Service
@RequiredArgsConstructor
public class JWTService {

    private final ObjectMapper objectMapper;

    private final SecurityEnvConfig securityEnvConfig;

    /**
     * @param userPayload
     * @return
     */
    public String generateAccessToken(UserPayload userPayload) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(securityEnvConfig.getSecret().getAccessToken().getKey());

            return JWT.create()
                    .withIssuedAt(Date.from(LocalDateTime.now()
                            .atZone(ZoneId.systemDefault())
                            .toInstant()))
                    .withIssuer(securityEnvConfig.getIssuer())
                    .withSubject(userPayload.getId())
                    .withAudience("user")
                    .withClaim("typ", Constants.BEARER_PREFIX)
                    .withPayload(objectMapper.readValue(objectMapper.writeValueAsString(userPayload), new TypeReference<Map<String, Object>>() {
                    }))
                    .withExpiresAt(Date.from(LocalDateTime.now().plus(securityEnvConfig.getSecret().getAccessToken().getTtl()).atZone(ZoneId.systemDefault()).toInstant()))
                    .sign(algorithm);
        } catch (JWTCreationException | JsonProcessingException | IllegalArgumentException exception) {
            throw new FoodException(HttpStatus.INTERNAL_SERVER_ERROR, CommonErrorCode.UNKNOWN, exception.getMessage());
        }
    }

    /**
     * @param userPayload
     * @return
     */
    public String generateRefreshToken(UserPayload userPayload) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(securityEnvConfig.getSecret().getRefreshToken().getKey());

            return JWT.create()
                    .withIssuedAt(Date.from(LocalDateTime.now()
                            .atZone(systemDefault())
                            .toInstant()))
                    .withIssuer(securityEnvConfig.getIssuer())
                    .withSubject(userPayload.getId())
                    .withAudience("user")
                    .withClaim("typ", Constants.REFRESH_PREFIX)
                    .withClaim("username", userPayload.getUsername())
                    .withExpiresAt(Date.from(LocalDateTime.now().plus(securityEnvConfig.getSecret().getRefreshToken().getTtl()).atZone(ZoneId.systemDefault()).toInstant()))
                    .sign(algorithm);
        } catch (JWTCreationException | IllegalArgumentException exception) {
            throw new FoodException(HttpStatus.INTERNAL_SERVER_ERROR, CommonErrorCode.UNKNOWN, "error when create refresh token");
        }
    }

    /**
     * @param token
     * @return
     */
    public DecodedJWT verifyAccessToken(String token) {
        Algorithm algorithm = Algorithm.HMAC256(securityEnvConfig.getSecret().getAccessToken().getKey());
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(securityEnvConfig.getIssuer())
                .withClaim("typ", Constants.BEARER_PREFIX)
                .build();

        return verifier.verify(token);
    }

    /**
     * @param token
     * @return
     */
    public DecodedJWT verifyRefreshToken(String token) {
        Algorithm algorithm = Algorithm.HMAC256(securityEnvConfig.getSecret().getRefreshToken().getKey());
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(securityEnvConfig.getIssuer())
                .withClaim("typ", Constants.REFRESH_PREFIX)
                .build();

        return verifier.verify(token);
    }

    /**
     * @param httpServletRequest
     * @return
     */
    public String resolveToken(HttpServletRequest httpServletRequest) {
        String bearerToken = httpServletRequest.getHeader(Constants.AUTHORIZATION_HEADER);

        if (bearerToken != null && bearerToken.startsWith(Constants.BEARER_PREFIX))
            return bearerToken.substring(Constants.BEARER_PREFIX.length()).trim();

        return null;
    }
}
