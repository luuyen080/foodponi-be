package org.foodponi.core.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@ConfigurationProperties("application.security.jwt")
@Getter
@Setter
public class SecurityEnvConfig {

    private String issuer;

    private JWTSecret secret;

    @Getter
    @Setter
    public static class JWTSecret {

        private SecretConfig accessToken;

        private SecretConfig refreshToken;

    }

    @Getter
    @Setter
    public static class SecretConfig {

        private String key;

        private Duration ttl;

    }

}
