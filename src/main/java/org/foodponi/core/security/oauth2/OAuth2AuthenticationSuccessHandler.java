//package org.foodponi.core.security.oauth2;
//
//import jakarta.servlet.http.Cookie;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import lombok.RequiredArgsConstructor;
//import org.foodponi.core.security.JWTService;
//import org.foodponi.business.user.IUserRepository;
//import org.foodponi.business.user.User;
//import org.foodponi.business.user.enums.Role;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
//import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
//import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//
//@Component
//@RequiredArgsConstructor
//public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
//    private final JWTService jwtService;
//    private final IUserRepository userRepository;
//    @Value("${application.security.jwt.refresh-token.expiration}")
//    private int expirationRefreshToken;
//
//    @Override
//    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
//        DefaultOAuth2User defaultOAuth2User = (DefaultOAuth2User) authentication.getPrincipal();
//
//        User tempUser = new User();
//        OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;
//        if (oAuth2AuthenticationToken.getAuthorizedClientRegistrationId().equals("google")) {
//            tempUser.setAvatar(defaultOAuth2User.getAttributes().get("picture").toString());
//            tempUser.setFirstName(defaultOAuth2User.getAttributes().get("family_name").toString());
//            tempUser.setLastName(defaultOAuth2User.getAttributes().get("given_name").toString());
//            tempUser.setEmail(defaultOAuth2User.getAttributes().get("email").toString());
//            tempUser.setProviderId(defaultOAuth2User.getAttributes().get("sub").toString());
//            tempUser.setAuthProvider(AuthProvider.google);
//            tempUser.setRole(Role.CUSTOMER);
//        } else if (oAuth2AuthenticationToken.getAuthorizedClientRegistrationId().equals("github")) {
//            tempUser.setAvatar(defaultOAuth2User.getAttributes().get("avatar_url").toString());
//            tempUser.setFirstName(defaultOAuth2User.getAttributes().get("name").toString());
//            tempUser.setProviderId(defaultOAuth2User.getAttributes().get("id").toString());
//            tempUser.setAuthProvider(AuthProvider.github);
//            tempUser.setRole(Role.CUSTOMER);
//        }
//        User user = userRepository.findByEmail(tempUser.getEmail())
//                .orElseGet(() -> userRepository.findByProviderId(tempUser.getProviderId())
//                        .orElseGet(() -> userRepository.save(tempUser)));
//        response.setContentType(MediaType.TEXT_HTML_VALUE);
//        PrintWriter out = response.getWriter();
//        Cookie cookie = new Cookie("access_token", jwtService.generateAccessToken(user));
//        cookie.setMaxAge(1);
//        cookie.setPath("/");
//        response.addCookie(cookie);
//        cookie = new Cookie("refresh_token", jwtService.generateRefreshToken(user));
//        cookie.setMaxAge(expirationRefreshToken / 1000);
//        cookie.setPath("/");
//        response.addCookie(cookie);
//        out.println("<!DOCTYPE html>\n" +
//                "<html>\n" +
//                "<head>\n" +
//                "</head>\n" +
//                "<body>\n" +
//                "<script>\n" +
//                "    window.close();\n" +
//                "</script>\n" +
//                "</body>\n" +
//                "</html>");
//        out.flush();
//        out.close();
//        response.setStatus(HttpStatus.OK.value());
//    }
//}