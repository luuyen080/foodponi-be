package org.foodponi.core.security.oauth2;

public enum AuthProvider {

    local,
    google,
    facebook,
    github

}
