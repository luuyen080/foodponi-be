package org.foodponi.core.interceptor;


import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
@RequiredArgsConstructor
public class CustomResponseBodyAdviceAdapter implements ResponseBodyAdvice<Object> {

    private final LoggingService loggingService;

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {

        ServletServerHttpRequest servletServerHttpRequest = (ServletServerHttpRequest) request;
        ServletServerHttpResponse servletServerHttpResponse = (ServletServerHttpResponse) response;

        loggingService.logResponse(servletServerHttpRequest.getServletRequest(), servletServerHttpResponse.getServletResponse(), body);

        // calculate execute time and add to response
        if (servletServerHttpRequest.getServletRequest().getAttribute("startTime") != null) {
            long startTime = (long) servletServerHttpRequest.getServletRequest().getAttribute("startTime");
            long timeElapsed = System.currentTimeMillis() - startTime;
            response.getHeaders().add("Elapsed-Time", String.valueOf(timeElapsed));
        }

        return body;
    }

}
