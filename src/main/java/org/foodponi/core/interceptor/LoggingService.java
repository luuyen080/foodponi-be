package org.foodponi.core.interceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Log4j2
public class LoggingService {

    private final ObjectMapper objectMapper;

    public void logRequest(HttpServletRequest httpServletRequest, Object body) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, String> parameters = buildParametersMap(httpServletRequest);

        stringBuilder.append("\nREQUEST: \n");
        stringBuilder.append("\tmethod=[").append(httpServletRequest.getMethod()).append("] \n");
        stringBuilder.append("\tpath=[").append(httpServletRequest.getRequestURI()).append("] \n");
        stringBuilder.append("\theaders=[").append(buildHeadersMap(httpServletRequest)).append("] \n");

        if (!parameters.isEmpty()) {
            stringBuilder.append("\tparameters=[").append(parameters).append("] \n");
        }

        if (body != null) {
            try {
                stringBuilder.append("\tbody=[").append(objectMapper.writeValueAsString(body)).append("]\n");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        log.info(stringBuilder.toString());
    }

    public void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                            Object body) {

        String stringBuilder = "\nRESPONSE: \n" +
                "\tmethod=[" + httpServletRequest.getMethod() + "] \n" +
                "\tpath=[" + httpServletRequest.getRequestURI() + "] \n" +
                "\tresponseHeaders=[" + buildHeadersMap(httpServletResponse) + "] \n";

        log.info(stringBuilder);
    }

    private Map<String, String> buildParametersMap(HttpServletRequest httpServletRequest) {
        Map<String, String> resultMap = new HashMap<>();
        Enumeration<String> parameterNames = httpServletRequest.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String key = parameterNames.nextElement();
            String value = httpServletRequest.getParameter(key);
            resultMap.put(key, value);
        }

        return resultMap;
    }

    private Map<String, String> buildHeadersMap(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        Enumeration<String> headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            String key = headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map;
    }

    private Map<String, String> buildHeadersMap(HttpServletResponse response) {
        Map<String, String> map = new HashMap<>();

        Collection<String> headerNames = response.getHeaderNames();

        for (String header : headerNames) {
            map.put(header, response.getHeader(header));
        }

        return map;
    }

}