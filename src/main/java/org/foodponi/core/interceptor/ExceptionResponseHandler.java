package org.foodponi.core.interceptor;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.ErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.method.MethodValidationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.HandlerMethodValidationException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Log4j2
public class ExceptionResponseHandler extends ResponseEntityExceptionHandler {

    /**
     * Handle when request body validation failure
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatusCode status, WebRequest request) {
        Map<String, Object> errorsDetail = ex.getBindingResult().getFieldErrors().stream()
                .collect(Collectors.toMap(FieldError::getField,
                        f -> Objects.requireNonNullElse(f.getDefaultMessage(), ""),
                        (msg1, msg2) -> msg1));

        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(),
                CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), errorsDetail);

        return new ResponseEntity<>(errorResponse, headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleHandlerMethodValidationException(HandlerMethodValidationException ex, HttpHeaders headers,
                                                                            HttpStatusCode status, WebRequest request) {
        Map<String, Object> details = new HashMap<>();
        ex.getAllValidationResults().forEach(parameterValidationResult ->
        {
            parameterValidationResult.getResolvableErrors().forEach(error -> {
                details.put(error.getCodes()[1], error.getDefaultMessage());
            });
        });

        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(), CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getMessage(), details);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle message not readable
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
                                                                  HttpStatusCode status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(), ex.getMessage(), Map.of());

        return new ResponseEntity<>(errorResponse, headers, status);
    }

    /**
     * Handle
     */
    @ExceptionHandler(value = {FoodException.class})
    protected ResponseEntity<Object> handle(FoodException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(ex.getErrorCode(), ex.getErrorMessage(), ex.getDetails());

        return new ResponseEntity<>(errorResponse, new HttpHeaders(), ex.getHttpStatus());
    }

    /**
     *
     */
    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    protected ResponseEntity<Object> handleConstraintException(DataIntegrityViolationException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCode.REQUEST_DATA_VALIDATION_FAILURE.getCode(), ex.getMessage(), Map.of());
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}
