package org.foodponi.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.foodponi.business.address.Address;
import org.foodponi.business.address.AddressDTO;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserPayload implements Serializable {

    private String id;

    private String username;

    private String email;

    private String role;

    private String avatar;

    private String authorities;

    private String addressId;

}
