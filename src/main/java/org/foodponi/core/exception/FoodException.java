package org.foodponi.core.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.enums.IErrorCode;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class FoodException extends RuntimeException {

    private final HttpStatus httpStatus;

    private final String errorCode;

    private final String errorMessage;

    private Map<String, Object> details;

    public FoodException(HttpStatus httpStatus, IErrorCode iErrorCode, String errorMessage) {
        this.httpStatus = httpStatus;
        this.errorCode = iErrorCode.getCode();
        this.errorMessage = errorMessage != null ? errorMessage : iErrorCode.getMessage();
    }

    public FoodException(IErrorCode iErrorCode, String errorMessage) {
        this.httpStatus = HttpStatus.BAD_REQUEST;
        this.errorCode = iErrorCode.getCode();
        this.errorMessage = errorMessage != null ? errorMessage : iErrorCode.getMessage();
    }

    public FoodException(HttpStatus httpStatus, IErrorCode iErrorCode) {
        this.httpStatus = httpStatus;
        this.errorCode = iErrorCode.getCode();
        this.errorMessage = iErrorCode.getMessage();
    }

    public FoodException(HttpStatus httpStatus, IErrorCode iErrorCode, Map<String, Object> details) {
        this.httpStatus = httpStatus;
        this.errorCode = iErrorCode.getCode();
        this.errorMessage = iErrorCode.getMessage();
        this.details = details;
    }

    public FoodException(IErrorCode iErrorCode) {
        this.httpStatus = HttpStatus.BAD_REQUEST;
        this.errorCode = iErrorCode.getCode();
        this.errorMessage = iErrorCode.getMessage();
    }

    public FoodException(String errorMessage) {
        this.httpStatus = HttpStatus.BAD_REQUEST;
        this.errorCode = CommonErrorCode.UNKNOWN.getCode();
        this.errorMessage = errorMessage;
    }

}
