package org.foodponi.core.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.security.SecureRandom;

public class Utils {

    private Utils() {

    }

    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";

    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();

    private static final String NUMBER = "0123456789";

    private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;

    public static String generateRandomString(int length, boolean onlyNumber) {
        if (length < 1) {
            throw new IllegalArgumentException();
        }

        StringBuilder sb = new StringBuilder(length);
        String dataForRandom = DATA_FOR_RANDOM_STRING;

        if (onlyNumber) {
            dataForRandom = NUMBER;
        }

        SecureRandom random = new SecureRandom();

        for (int i = 0; i < length; i++) {
            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(dataForRandom.length());
            char rndChar = dataForRandom.charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();
    }

    public static Pageable getPageable(Integer page, Integer pageSize) {
        return (page != null && pageSize != null) ? PageRequest.of(page, pageSize) : Pageable.unpaged();
    }

}
