package org.foodponi.core.util;

import org.foodponi.core.enums.IErrorCode;
import org.foodponi.core.exception.FoodException;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;

public final class Validate {

    private Validate() {
        
    }
    
    /**
     * 
     * @param object
     * @param iErrorCode
     */
    public static void notNullOrEmpty(Object object, IErrorCode iErrorCode) {
        if (ObjectUtils.isEmpty(object)) {
            throw new FoodException(iErrorCode);
        }
    }

    /**
     * 
     * @param object
     * @param httpStatus
     * @param iErrorCode
     */
    public static void notNullOrEmpty(Object object, HttpStatus httpStatus, IErrorCode iErrorCode) {
        if (ObjectUtils.isEmpty(object)) {
            throw new FoodException(httpStatus, iErrorCode);
        }
    }

    /**
     * 
     * @param obj1
     * @param obj2
     * @param iErrorCode
     */
    public static void mustEqual(Object obj1, Object obj2, IErrorCode iErrorCode) {
        if (!obj1.equals(obj2)) {
            throw new FoodException(iErrorCode);
        }
    }

    /**
     *
     * @param obj1
     * @param obj2
     * @param iErrorCode
     */
    public static void mustEqual(Object obj1, Object obj2, HttpStatus httpStatus, IErrorCode iErrorCode) {
        if (!obj1.equals(obj2)) {
            throw new FoodException(httpStatus, iErrorCode);
        }
    }

    /**
     *
     * @param obj
     * @param iErrorCode
     */
    public static void mustTrue(Object obj, IErrorCode iErrorCode) {
        if (!Boolean.TRUE.equals(obj)) {
            throw new FoodException(iErrorCode);
        }
    }

    /**
     *
     * @param obj
     * @param iErrorCode
     */
    public static void mustTrue(Object obj, HttpStatus httpStatus, IErrorCode iErrorCode) {
        if (!Boolean.TRUE.equals(obj)) {
            throw new FoodException(httpStatus, iErrorCode);
        }
    }

    /**
     *
     * @param obj
     * @param iErrorCode
     */
    public static void mustFail(Object obj, IErrorCode iErrorCode) {
        if (Boolean.TRUE.equals(obj)) {
            throw new FoodException(iErrorCode);
        }
    }

    /**
     *
     * @param obj
     * @param iErrorCode
     */
    public static void mustFail(Object obj, HttpStatus httpStatus, IErrorCode iErrorCode) {
        if (Boolean.TRUE.equals(obj)) {
            throw new FoodException(httpStatus, iErrorCode);
        }
    }

}
