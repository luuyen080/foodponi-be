package org.foodponi.core.util;

import lombok.extern.log4j.Log4j2;
import org.foodponi.business.user.enums.RoleErrorCode;
import org.foodponi.core.enums.IPrivilege;
import org.foodponi.core.enums.Module;
import org.foodponi.core.exception.FoodException;
import org.springframework.util.ObjectUtils;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class PermissionParser {

    private PermissionParser() {

    }

    private static final String DELIMITER = "_";

    /**
     *
     * @param privilegeEnumsValue
     * @return
     */
    public static String convertHexFromSetInt(Set<Integer> privilegeEnumsValue) {
        // Begin calculate hex permission
        BigInteger decimalPermission = BigInteger.ZERO;

        for (Integer permissionEnumValue: privilegeEnumsValue) {
            // Calculate permission as decimal value
            BigInteger numTwo = BigInteger.TWO;
            decimalPermission = decimalPermission.add(numTwo.pow(permissionEnumValue));
        }

        return decimalPermission.toString(16);
    }

    /**
     *
     * @param hexValue
     * @return
     */
    public static Set<Integer> convertSetIntFromHex(String hexValue) {
        // Convert hex string to binary string
        String binaryPermission = new BigInteger(hexValue, 16).toString(2);
        // Reverse binary string
        binaryPermission = new StringBuilder(binaryPermission).reverse().toString();

        Set<Integer> lst = new HashSet<>();

        for (int j = 0; j < binaryPermission.length(); j++) {
            if (binaryPermission.charAt(j) == '1') {
                lst.add(j);
            }
        }

        return lst;
    }

    /**
     *
     * @param modulePrivileges
     * @return
     */
    public static String buildAuthoritiesHex(Map<String, Set<String>> modulePrivileges) {
        List<String> authoritiesHex = new ArrayList<>();

        Map<Integer, Set<Integer>> mapPrivilegesInt = new HashMap<>();
        Map<Module, Set<String>> mapPrivilegeByModule = new HashMap<>();
        Map<String, Module> mapModuleByName = new HashMap<>();

        for (Module module: Module.values()) {
            mapPrivilegeByModule.put(module, module.privileges().stream().map(IPrivilege::name).collect(Collectors.toSet()));
            mapModuleByName.put(module.name(), module);
        }

        for (Map.Entry<String, Set<String>> entry: modulePrivileges.entrySet()) {
            String moduleName = entry.getKey();
            Set<String> privileges = entry.getValue();
            Module module = mapModuleByName.get(moduleName);

            if (!mapPrivilegeByModule.containsKey(module) || !mapPrivilegeByModule.get(module).containsAll(privileges)) {
                throw new FoodException(RoleErrorCode.MODULE_PRIVILEGES_NOT_FOUND);
            }

            Set<Integer> privilegesInt = module.privileges().stream().filter(p -> privileges.contains(p.name())).map(IPrivilege::value).collect(Collectors.toSet());

            mapPrivilegesInt.put(module.value(), privilegesInt);
        }

        Set<Integer> resourceKeys = Stream.of(Module.values()).map(Module::value).collect(Collectors.toCollection(TreeSet::new));

        for (Integer key: resourceKeys) {
            if (mapPrivilegesInt.containsKey(key)) {
                authoritiesHex.add(convertHexFromSetInt(mapPrivilegesInt.get(key)));
            } else {
                authoritiesHex.add("");
            }
        }

        return String.join(DELIMITER, authoritiesHex);
    }

    /**
     *
     * @param permissionsHex
     * @return
     */
    public static Map<Integer, Set<Integer>> getPermissionsByHex(String permissionsHex) {
        Map<Integer, Set<Integer>> permissions = new HashMap<>();

        if (ObjectUtils.isEmpty(permissionsHex)) {
            return Map.of();
        }

        String[] permissionHexArr = permissionsHex.split(DELIMITER);

        for (int i = 0; i < permissionHexArr.length; i++) {
            String permissionHex = permissionHexArr[i];

            if (ObjectUtils.isEmpty(permissionHex)) {
                continue;
            }

            Set<Integer> privileges = convertSetIntFromHex(permissionHex);

            permissions.put(i + 1, privileges);
        }

        return permissions;
    }

    /**
     *
     * @param authoritiesHex
     * @return
     */
    public static Map<String, Set<String>> convertAuthoritiesHexToModulePrivileges(String authoritiesHex) {
        Map<Integer, Set<Integer>> permissions = getPermissionsByHex(authoritiesHex);

        if (ObjectUtils.isEmpty(permissions)) {
            return Map.of();
        }

        Map<Integer, String> mapResourceByName = Stream.of(Module.values()).collect(Collectors.toMap(Module::value, Module::name));
        Map<Integer, Map<Integer, String>> mapPermissionByResource =
                Stream.of(Module.values())
                        .collect(Collectors.toMap(
                                Module::value,
                                r -> r.privileges().stream().collect(Collectors.toMap(IPrivilege::value, IPrivilege::name))
                        ));

        Map<String, Set<String>> result = new HashMap<>();

        for (Map.Entry<Integer, Set<Integer>> entry: permissions.entrySet()) {
            Integer moduleVal = entry.getKey();
            Set<Integer> privileges = entry.getValue();

            String moduleName = mapResourceByName.get(moduleVal);
            Set<String> privilegesName = privileges.stream().map(p -> mapPermissionByResource.get(moduleVal).get(p)).collect(Collectors.toSet());

            result.put(moduleName, privilegesName);
        }

        return result;
    }

    /**
     *
     * @param authoritiesHex
     * @return
     */
    public static List<String> handlePermissionAuthorities(String authoritiesHex) {
        Map<String, Set<String>> modulePrivileges = convertAuthoritiesHexToModulePrivileges(authoritiesHex);

        // List permission
        List<String> authorities = new ArrayList<>();

        for (Map.Entry<String, Set<String>> entry: modulePrivileges.entrySet()) {
            String moduleName = entry.getKey();
            Set<String> privilegesName = entry.getValue();

            for (String privilege: privilegesName) {
                authorities.add(privilege + DELIMITER + moduleName);
            }
        }

        return authorities;
    }

}
