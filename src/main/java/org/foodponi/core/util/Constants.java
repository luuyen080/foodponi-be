package org.foodponi.core.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;

public class Constants {

    private Constants() {
        
    }
      
    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String AUTHORIZATION_REFRESH_HEADER = "Refresh-Token";

    public static final String USER_PAYLOAD_HEADER = "X-User-Payload";

    public static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";

    public static final String GRANT_TYPE_CLIENT = "client_credentials";

    public static final String X_USER_ACCESS_TYPE_HEADER = "X-User-Access-Type";

    public static final String REFRESH_PREFIX = "Refresh";
    public static final String BEARER_PREFIX = "Bearer";
    public static final String BASIC_SCHEMA = "Basic";
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";

    public static final String DEFAULT_QUERY_NAME = "find";

    public static final YearMonth MIN_MONTH_YEAR = YearMonth.of(0, 1);
    public static final YearMonth MAX_MONTH_YEAR = YearMonth.of(9999, 12);

    public static final String MIN_LOCAL_DATE_STRING = "0000-01-01";
    public static final String MAX_LOCAL_DATE_STRING = "9999-12-31";

    public static final LocalDate MIN_LOCAL_DATE = LocalDate.of(0, 1, 1);
    public static final LocalDate MAX_LOCAL_DATE = LocalDate.of(9999, 12, 31);

    public static final String ROOT_VALUE = "root";

    public static final BigDecimal DEFAULT_TAX_RATE = BigDecimal.valueOf(10);
    public static final BigDecimal MAX_PERCENT = BigDecimal.valueOf(100);

}
