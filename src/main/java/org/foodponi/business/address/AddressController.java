package org.foodponi.business.address;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/addresses")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public Page<AddressDTO.AddressResponseDTO> getAddressesPage(
            Authentication authentication,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer pageSize) {

        return addressService.getAddressesPage(authentication.getName(), Utils.getPageable(page, pageSize));
    }

    @GetMapping("/{addressId}")
    public AddressDTO.AddressResponseDTO getAddressById(
            Authentication authentication,
            @PathVariable String addressId) {

        return addressService.getAddressById(authentication.getName(), addressId);
    }

    @PostMapping
    public AddressDTO.AddressResponseDTO createAddress(
            Authentication authentication,
            @RequestBody @Valid AddressDTO.AddressRequestDTO addressRequestDTO) {

        return addressService.create(authentication.getName(), addressRequestDTO);
    }

    @PatchMapping(value = "/{addressId}")
    public AddressDTO.AddressResponseDTO updateAddressById(
            @PathVariable String addressId,
            @RequestBody Map<String, Object> fields) {

        return addressService.updateAddressById(addressId, fields);
    }

    @DeleteMapping(value = "/{addressId}")
    public ResponseEntity<?> deleteAddressById(
            Authentication authentication,
            @PathVariable String addressId) {

        addressService.deleteAddressById(authentication.getName(), addressId);
        return ResponseEntity.noContent().build();
    }

}