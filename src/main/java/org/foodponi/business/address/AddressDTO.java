package org.foodponi.business.address;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.foodponi.business.user.UserDTO;

public abstract class AddressDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class AddressRequestDTO {

        @NotBlank
        private String fullName;

        @NotBlank
        private String phoneNumber;

        @NotBlank
        private String address;

        private double lon;

        private double lat;

    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class AddressIdDTO {

        @NotNull
        @Size(min = 36, max = 36)
        private String id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AddressResponseDTO {

        private String id;

        private String fullName;

        private String phoneNumber;

        private String address;

        private double lon;

        private double lat;

        @JsonIgnoreProperties(value = "address")
        private UserDTO.UserResponseDTO user;

    }

}

