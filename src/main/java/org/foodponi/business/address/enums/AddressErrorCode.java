package org.foodponi.business.address.enums;

import org.foodponi.core.enums.IErrorCode;

public enum AddressErrorCode implements IErrorCode {

    ADDRESS_NOT_FOUND("A01", "Address not found"),

    FULLNAME_IS_EMPTY("A03", "Fullname is empty"),
    PHONE_NUMBER_IS_EMPTY("A04", "Phone number is empty"),
    ADDRESS_IS_EMPTY("A05", "Address is empty"),
    FULLNAME_IS_NOT_NULL("A06", "Fullname is not null"),
    PHONE_NUMBER_IS_NOT_NULL("A07", "Phone number is not null"),
    ADDRESS_IS_NOT_NULL("A08", "Address is not null"),
    FULLNAME_AND_ADDRESS_AND_PHONENUMBER_IS_NOT_NULL("A09", "Fullname and address and phone number is not null"),

    FULLNAME_AND_ADDRESS_AND_PHONE_NUMBER_IS_NOT_EMPTY("A10", "Fullname and address and phone number is not empty"),
    ADDRESS_EXISTED("A02", "Address existed");

    AddressErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
