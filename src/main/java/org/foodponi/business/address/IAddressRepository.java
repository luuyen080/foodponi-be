package org.foodponi.business.address;

import org.foodponi.business.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IAddressRepository extends JpaRepository<Address, String> {

    boolean existsByFullNameAndPhoneNumberAndAddressAndUserId(String fullName, String phoneNumber, String address, String userId);

    Page<Address> findAllByUserId(Pageable pageable, String id);
}
