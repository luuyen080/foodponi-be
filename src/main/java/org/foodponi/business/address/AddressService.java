package org.foodponi.business.address;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.address.enums.AddressErrorCode;
import org.foodponi.business.user.IUserRepository;
import org.foodponi.business.user.User;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.security.UserContext;
import org.foodponi.core.util.MapperUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class AddressService {

    private final IAddressRepository addressRepository;

    private final IUserRepository userRepository;

    private final UserContext userContext;

    /**
     * @param userId
     * @param addressRequestDTO
     * @return
     */
    public AddressDTO.AddressResponseDTO create(String userId, AddressDTO.AddressRequestDTO addressRequestDTO) {
        try {
            log.info("AddressService::create execution started...");

            // check existed
            if (addressRepository.existsByFullNameAndPhoneNumberAndAddressAndUserId(
                    addressRequestDTO.getFullName(),
                    addressRequestDTO.getPhoneNumber(),
                    addressRequestDTO.getAddress(),
                    userId
            )) throw new FoodException(AddressErrorCode.ADDRESS_EXISTED);

            Address address = MapperUtils.map(addressRequestDTO, Address.class);
            address.setUser(User.builder().id(userId).build());

            log.info("AddressService::create execution ended...");
            return MapperUtils.map(addressRepository.save(address), AddressDTO.AddressResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting address to database , Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @return
     */
    public Page<AddressDTO.AddressResponseDTO> getAddressesPage(String userId, Pageable pageable) {
        Page<Address> productsResponse;
        try {
            log.info("AddressService::getAddressesPage execution started...");

            productsResponse = addressRepository.findAllByUserId(pageable, userId);

        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("AddressService::getAddressesPage execution ended...");
        return MapperUtils.mapPage(productsResponse, AddressDTO.AddressResponseDTO.class);
    }

    /**
     * @param addressId
     * @param fields
     * @return
     */
    public AddressDTO.AddressResponseDTO updateAddressById(String addressId, Map<String, Object> fields) {
        try {
            log.info("AddressService::updateAddressById execution started...");

            //CHECK EXISTED
            Address existAddress = addressRepository.findById(addressId).orElseThrow(() -> new FoodException(AddressErrorCode.ADDRESS_NOT_FOUND));

            //EXECUTE
            fields.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(Address.class, key);
                field.setAccessible(true);
                if (!key.equals("id")) ReflectionUtils.setField(field, existAddress, value);
            });

            Address addressResult = addressRepository.save(existAddress);

            log.info("AddressService::updateAddressById execution ended...");
            return MapperUtils.map(addressResult, AddressDTO.AddressResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting address to database, Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param addressId
     */
    public void deleteAddressById(String userId, String addressId) {
        try {
            log.info("AddressService::deleteAddressById execution started...");

            //CHECK EXIST
            Address existAddress = addressRepository.findById(addressId).orElseThrow(() -> new FoodException(AddressErrorCode.ADDRESS_NOT_FOUND));

            //CHECK USER IN ADDRESS
            if (!existAddress.getUser().getId().equals(userId)) {
                throw new FoodException(CommonErrorCode.ACCESS_FORBIDDEN);
            }

            //EXECUTE
            addressRepository.delete(existAddress);

            log.info("AddressService::deleteAddressById execution ended...");
        } catch (FoodException ex) {
            log.error("Exception occurred while deleting address {} from database, Exception message {}", addressId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param addressId
     * @return
     */
    public AddressDTO.AddressResponseDTO getAddressById(String userId, String addressId) {
        try {
            log.info("AddressService::getAddressById execution started...");

            //CHECK EXIST
            Address existAddress = addressRepository.findById(addressId).orElseThrow(() -> new FoodException(AddressErrorCode.ADDRESS_NOT_FOUND));

            //CHECK USER IN ADDRESS
            if (!existAddress.getUser().getId().equals(userId)) {
                throw new FoodException(CommonErrorCode.ACCESS_FORBIDDEN);
            }

            log.info("AddressService::getAddressById execution ended...");
            return MapperUtils.map(existAddress, AddressDTO.AddressResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving address {} from database , Exception message {}", addressId, ex.getMessage());
            throw ex;
        }
    }

}