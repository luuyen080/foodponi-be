package org.foodponi.business.address;

import jakarta.persistence.*;
import lombok.*;
import org.foodponi.business.entitybase.AbstractEntity;
import org.foodponi.business.user.User;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "addresses")
public class Address extends AbstractEntity {

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "lon")
    private double lon;

    @Column(name = "lat")
    private double lat;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

}
