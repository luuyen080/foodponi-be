package org.foodponi.business.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.foodponi.business.payment.enums.PaymentMethod;
import org.foodponi.business.payment.enums.PaymentStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInfo {

    private String id = RandomStringUtils.randomAlphabetic(6).toLowerCase();

    private PaymentMethod method = PaymentMethod.CASH;

    private PaymentStatus status = PaymentStatus.PAYING;

}
