package org.foodponi.business.payment.enums;

public enum PaymentStatus {

    PAYING,
    PAID,
    FAILED

}
