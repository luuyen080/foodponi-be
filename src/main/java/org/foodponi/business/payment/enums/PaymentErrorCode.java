package org.foodponi.business.payment.enums;

import org.foodponi.core.enums.IErrorCode;

public enum PaymentErrorCode implements IErrorCode {

    PAYMENT_NOT_FOUND("PM01", "payment not found");

    PaymentErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
