package org.foodponi.business.payment.enums;

public enum PaymentMethod {

    CASH,

    VNPAY,

    MOMO

}
