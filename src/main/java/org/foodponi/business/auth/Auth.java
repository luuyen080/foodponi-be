package org.foodponi.business.auth;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public abstract class Auth {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationResponse {

        private String accessToken;

        private String refreshToken;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class AuthenticationRequest {

        @Length(min = 5, max = 50)
        private String username;

        @Email
        private String email;

        @NotNull
        @Length(min = 6, max = 50)
        private String password;

    }

}