package org.foodponi.business.auth;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public Auth.AuthenticationResponse login(
            @RequestBody @Valid Auth.AuthenticationRequest authenticationRequest) {

        return authService.login(authenticationRequest);
    }

    @PostMapping("/refresh-token")
    public Auth.AuthenticationResponse refreshToken(
            @RequestBody Map<String, String> token) {

        return authService.refreshToken(token.get("refreshToken"));
    }

}