package org.foodponi.business.auth;

import lombok.RequiredArgsConstructor;
import org.foodponi.business.user.IUserRepository;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.exception.FoodException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class AuthConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
