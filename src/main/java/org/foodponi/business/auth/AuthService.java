package org.foodponi.business.auth;

import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.address.AddressDTO;
import org.foodponi.business.user.IUserRepository;
import org.foodponi.business.user.User;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.UserPayload;
import org.foodponi.core.security.JWTService;
import org.foodponi.core.util.MapperUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthService {

    private final IUserRepository userRepository;

    private final JWTService jwtService;

    private final PasswordEncoder passwordEncoder;

    /**
     *
     * @param authenticationRequest
     * @return
     */
    public Auth.AuthenticationResponse login(Auth.AuthenticationRequest authenticationRequest) {
        try {
            log.info("AuthService::login execution started...");

            User user = userRepository.findByUsername(authenticationRequest.getUsername())
                    .orElseGet(() -> userRepository.findByEmail(authenticationRequest.getEmail())
                            .orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND)));

            if (!user.isStatus()) throw new FoodException(UserErrorCode.USER_BLOCKED);

            if (!passwordEncoder.matches(authenticationRequest.getPassword(), user.getPassword()))
                throw new FoodException(UserErrorCode.CURRENT_PASSWORD_NOT_MATCH);

            UserPayload userPayload = UserPayload
                    .builder()
                    .id(user.getId())
                    .username(user.getUsername())
                    .email(user.getEmail())
                    .role(user.getRole().name())
                    .avatar(user.getAvatar())
                    .authorities(user.getRole().getAuthorities().toString())
                    .addressId(user.getAddress() != null ? user.getAddress().getId() : null)
                    .build();
            userPayload.setAuthorities(String.valueOf(user.getRole().getAuthorities()));

            log.info("AuthService::login execution ended...");
            return new Auth.AuthenticationResponse(jwtService.generateAccessToken(userPayload), jwtService.generateRefreshToken(userPayload));
        } catch (FoodException ex) {
            log.error("Exception occurred while logging in account, Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     *
     * @param refreshToken
     * @return
     */
    public Auth.AuthenticationResponse refreshToken(String refreshToken) {
        log.info("AuthService::refresh token execution started...");

        DecodedJWT decodedJWT = jwtService.verifyRefreshToken(refreshToken);
        String userId = decodedJWT.getClaim("sub").asString();

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

        if (!user.isStatus())
            throw new FoodException(UserErrorCode.USER_BLOCKED);

        UserPayload userPayload = MapperUtils.map(user, UserPayload.class);

        log.info("AuthService::refresh token execution ended...");
        return new Auth.AuthenticationResponse(jwtService.generateAccessToken(userPayload), jwtService.generateRefreshToken(userPayload));
    }

}
