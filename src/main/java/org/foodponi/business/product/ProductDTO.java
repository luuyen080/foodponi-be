package org.foodponi.business.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.foodponi.business.order_item.OrderItemDTO;
import org.foodponi.business.product_category.CategoryDTO;
import org.foodponi.business.product_detail.ProductDetailDTO;
import org.foodponi.business.user.UserDTO;

import java.sql.Timestamp;
import java.util.List;

public abstract class ProductDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ProductCreateRequestDTO {

        @NotBlank
        private String name;

        private String slug;

        private String shortDescription;

        private String thumbnail;

        private boolean status;

        @Valid
        @NotNull
        @Size(min = 1)
        private List<ProductDetailDTO.ProductDetailCreateRequestDTO> productDetails;

        private List<CategoryDTO.CategoryIdDTO> categories;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ProductUpdateRequestDTO {

        @Size(min = 36, max = 36)
        @NotNull
        private String id;

        @NotBlank
        private String name;

        private String slug;

        private String shortDescription;

        private String thumbnail;

        @Valid
        @NotNull
        @NotEmpty
        private List<ProductDetailDTO.ProductDetailUpdateRequestDTO> productDetails;

        private List<CategoryDTO.CategoryIdDTO> categories;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ProductIdDTO {

        @NotBlank
        private String id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    public static class ProductResponseDTO {

        private String id;

        private String name;

        private String slug;

        private String shortDescription;

        private String thumbnail;

        private boolean status;

        private UserDTO.UserResponseDTO user;

        private double rate;

        private int rateCount;

        @JsonIgnoreProperties(value = "product")
        private List<ProductDetailDTO.ProductDetailResponseDTO> productDetails;

        @JsonIgnoreProperties(value = "categories")
        private List<CategoryDTO.CategoryResponseDTO> categories;

    }

}

