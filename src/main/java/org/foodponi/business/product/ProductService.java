package org.foodponi.business.product;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.order_item.IOrderItemRepository;
import org.foodponi.business.order_item.OrderItem;
import org.foodponi.business.order_item.OrderItemDTO;
import org.foodponi.business.order_item.Rate;
import org.foodponi.business.product.enums.ProductErrorCode;
import org.foodponi.business.product_category.Category;
import org.foodponi.business.product_category.ICategoryRepository;
import org.foodponi.business.product_category.enums.CategoryErrorCode;
import org.foodponi.business.product_detail.IProductDetailRepository;
import org.foodponi.business.product_detail.ProductDetailDTO;
import org.foodponi.business.product_detail.enums.ProductDetailErrorCode;
import org.foodponi.business.user.User;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.security.UserContext;
import org.foodponi.core.util.MapperUtils;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final IProductRepository productRepository;

    private final IProductDetailRepository productDetailRepository;

    private final IOrderItemRepository orderItemRepository;

    private final ICategoryRepository categoryRepository;

    private final UserContext userContext;

    /**
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    public Page<ProductDTO.ProductResponseDTO> getProductsPage(Integer page, Integer pageSize, Boolean status, String categoryId) {
        try {
            log.info("ProductService::getProductsPage execution started...");

            Page<Product> productsResponse;

            if (status != null) {
                if (categoryId != null) {
                    Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new FoodException(CategoryErrorCode.CATEGORY_NOT_FOUND));
                    productsResponse = productRepository.findAllByStatusAndCategories(Utils.getPageable(page, pageSize), status, category);
                } else
                    productsResponse = productRepository.findAllByStatus(Utils.getPageable(page, pageSize), status);
            } else
                productsResponse = productRepository.findAll(Utils.getPageable(page, pageSize));

            log.info("ProductService::getProductsPage execution ended...");

            Page<ProductDTO.ProductResponseDTO> productResponseDTOPage = MapperUtils.mapPage(productsResponse, ProductDTO.ProductResponseDTO.class);
            productResponseDTOPage.getContent().forEach(productDTO -> {
                Map<String, Double> rateAverageAndSales = getRateProductAverageAndCountByIdProduct(productDTO.getId());
                productDTO.setRate(rateAverageAndSales.get("rateAverage"));
                productDTO.setRateCount(rateAverageAndSales.get("rateCount").intValue());
            });

            return productResponseDTOPage;
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving products from database, Exception message {}", ex.getMessage());
            throw ex;
        }
    }


    /**
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    public Page<ProductDTO.ProductResponseDTO> getProductsPageByRetailer(String userId, Integer page, Integer pageSize, Boolean status) {
        Page<Product> productsResponse;

        try {
            log.info("ProductService::getProductsPageByRetailer execution started...");

            productsResponse = status != null
                    ? productRepository.findAllByUserIdAndStatus(Utils.getPageable(page, pageSize), userId, status)
                    : productRepository.findAllByUserId(Utils.getPageable(page, pageSize), userId);

        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("OrderService::getProductsPageByRetailer execution ended...");
        return MapperUtils.mapPage(productsResponse, ProductDTO.ProductResponseDTO.class);
    }

    /**
     * @param productId
     * @return
     */
    public ProductDTO.ProductResponseDTO getProductById(String productId) {
        log.info("ProductService::getProductById execution started...");
        Optional<Product> productOptional = productRepository.findById(productId);
        Product product = productOptional.orElseThrow(() -> new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND));
        log.info("ProductService::getProductById execution ended...");

        ProductDTO.ProductResponseDTO productResponseDTO = MapperUtils.map(product, ProductDTO.ProductResponseDTO.class);

        productResponseDTO.getProductDetails().forEach(productDetail -> {
            String productDetailId = productDetail.getId();
            Map<String, Double> averageRateAndSales = getRateProductDetailAverageByIdProductDetail(productDetailId);
            productDetail.setRate(averageRateAndSales.get("rateAverage"));
            productDetail.setSales((averageRateAndSales.get("sales").intValue()));
            productDetail.setRateCount(averageRateAndSales.get("rateCount").intValue());
        });

        return productResponseDTO;
    }


    /**
     * @param userId
     * @param productId
     * @return
     */
    public ProductDTO.ProductResponseDTO getProductByIdAndRetailer(String userId, String productId) {
        log.info("ProductService::getProductByIdAndRetailer execution started...");

        Product product = productRepository.findByIdAndUserId(productId, userId).orElseThrow(() -> new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND));

        log.info("ProductService::getProductByIdAndRetailer execution ended...");
        return MapperUtils.map(product, ProductDTO.ProductResponseDTO.class);
    }

    /**
     * @param userId
     * @param productCreateRequestDTO
     * @return
     */
    public ProductDTO.ProductResponseDTO create(String userId, ProductDTO.ProductCreateRequestDTO productCreateRequestDTO) {
        try {
            log.info("ProductService::create execution started...");

            // check duplicate product detail's name
            long distinctLength = productCreateRequestDTO.getProductDetails().stream().map(productDetails -> {
                if (productDetails == null) throw new FoodException(ProductDetailErrorCode.PRODUCT_DETAIL_EMPTY);
                return productDetails.getName();
            }).distinct().count();
            if (distinctLength != productCreateRequestDTO.getProductDetails().size())
                throw new FoodException(ProductDetailErrorCode.PRODUCT_DETAIL_NAME_DUPLICATED);

            // check slug is existed
            if (productRepository.existsBySlug(productCreateRequestDTO.getSlug()))
                throw new FoodException(ProductErrorCode.PRODUCT_SLUG_EXISTED);

            // Set retailer for product
            Product product = MapperUtils.map(productCreateRequestDTO, Product.class);
            product.setUser(User.builder().id(userId).build());

            Product productResult = productRepository.save(product);

            // set product into product detail
            productResult.getProductDetails().forEach(productDetail -> {
                productDetail.setProduct(productResult);
                productDetailRepository.save(productDetail);
            });

            log.info("ProductService::create execution ended...");
            return MapperUtils.map(productResult, ProductDTO.ProductResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting product to database , Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param productId
     */
    public void deleteProductByAdmin(String productId) {
        try {
            log.info("ProductService::deleteProductByAdmin execution started...");

            // check existed
            Product existProduct = productRepository.findById(productId).orElseThrow(() -> new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND));

            // execute
            if (existProduct.isStatus()) {
                existProduct.setStatus(false);
                productRepository.save(existProduct);
            } else throw new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND);

            log.info("ProductService::deleteProductByAdmin execution ended...");
        } catch (FoodException ex) {
            log.error("Exception occurred while deleting product {} from database, Exception message {}", productId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param productId
     */
    public void deleteProductByRetailer(String userId, String productId) {
        try {
            log.info("ProductService::deleteProductByRetailer execution started...");

            // check existed
            Product existProduct = productRepository.findByIdAndUserId(productId, userId).orElseThrow(() -> new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND));

            // execute
            if (existProduct.isStatus()) {
                existProduct.setStatus(false);
                productRepository.save(existProduct);
            } else throw new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND);

            log.info("ProductService::deleteProductByRetailer execution ended...");
        } catch (FoodException ex) {
            log.error("Exception occurred while deleting product {} from database, Exception message {}", productId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param productDetailId
     * @return
     */
    public List<OrderItemDTO.RateResponseDTO> getRateProductByIdDetail(String productDetailId) {
        log.info("ProductService::getProductById execution started...");
//        productRepository.findById(productDetailId).orElseThrow(() -> new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND));

        // Lấy danh sách các order items liên quan đến sản phẩm có productDetailId
        List<OrderItem> orderItems = orderItemRepository.findAll().stream()
                .filter(orderItem -> orderItem.getProductDetail().getId().equals(productDetailId))
                .collect(Collectors.toList());

        List<OrderItemDTO.RateResponseDTO> rateResponseDTOList = new ArrayList<>();

        orderItems.forEach(orderItem -> {
            if (orderItem.getRate() != null) {
                OrderItemDTO.RateResponseDTO rateDto = new OrderItemDTO.RateResponseDTO().builder()
                        .rate(orderItem.getRate().getRate())
                        .message(orderItem.getRate().getMessage())
                        .images(orderItem.getRate().getImages())
                        .username(orderItem.getOrder().getUser().getUsername())
                        .avatar(orderItem.getOrder().getUser().getAvatar())
                        .build();
                rateResponseDTOList.add(rateDto);
            }
        });

        log.info("ProductService::getProductById execution ended...");
        return rateResponseDTOList;
    }

    /**
     * @param productDetailId
     * @return
     */
    public Map<String, Double> getRateProductDetailAverageByIdProductDetail(String productDetailId) {
        log.info("ProductService::getRateProducAverageByIdProductDetail...");
        // Lấy danh sách các order items liên quan đến sản phẩm detail có productDetailId
        List<OrderItem> orderItems = orderItemRepository.findAll().stream()
                .filter(orderItem -> orderItem.getProductDetail().getId().equals(productDetailId))
                .collect(Collectors.toList());
        int rateTotal = 0;
        int rateCount = 0;

        for (OrderItem orderItem : orderItems) {
            Rate rate = orderItem.getRate();
            if (rate != null) {
                rateTotal += rate.getRate();
                rateCount++;
            }
        }
        Map<String, Double> rateAvarageAndSales = new HashMap<>();
        rateAvarageAndSales.put("rateAverage", rateCount == 0 ? 0 : (double) rateTotal / rateCount);
        rateAvarageAndSales.put("sales", Double.valueOf(orderItems.size()));
        rateAvarageAndSales.put("rateCount", Double.valueOf(rateCount));
        log.info("ProductService::getRateProductDetailAverageByIdProductDetail execution end...");
        return rateAvarageAndSales;
    }

    /**
     * @param productId
     * @return
     */
    public Map<String, Double> getRateProductAverageAndCountByIdProduct(String productId) {
        log.info("ProductService::getRateProductAverageByIdProduct...");
        // Lấy danh sách các order items liên quan đến sản phẩm có productId
        List<OrderItem> orderItems = orderItemRepository.findAll().stream()
                .filter(orderItem -> orderItem.getProductDetail().getProduct().getId().equals(productId))
                .collect(Collectors.toList());
        double rateTotal = 0;
        int rateCount = orderItems.size();

        for (OrderItem orderItem : orderItems) {
            Rate rate = orderItem.getRate();
            if (rate != null) {
                rateTotal += rate.getRate();
            } else {
                rateCount--;
            }
        }

        log.info("ProductService::getRateProductAverageByIdProduct execution end...");
        double rateAverage = rateCount == 0 ? 0 : rateTotal / rateCount;
        Map<String, Double> rateAvarageAndSales = new HashMap<>();
        rateAvarageAndSales.put("rateAverage", rateAverage);
        rateAvarageAndSales.put("rateCount", Double.valueOf(rateCount));
        return rateAvarageAndSales;
    }

}