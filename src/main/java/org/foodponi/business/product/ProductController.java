package org.foodponi.business.product;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.foodponi.business.order_item.OrderItemDTO;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final IProductRepository productRepository;

    @GetMapping(value = "/retailer/products")
    public Page<ProductDTO.ProductResponseDTO> getProductsPageByRetailer(
            Authentication authentication,
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize,
            @RequestParam(required = false) Boolean status) {
        return productService.getProductsPageByRetailer(authentication.getName(), page, pageSize, status);
    }

    @GetMapping(value = "/products")
    public Page<ProductDTO.ProductResponseDTO> getProductsPage(
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize,
            @RequestParam(required = false) Boolean status,
            @RequestParam(required = false) String categoryId) {
        return productService.getProductsPage(page, pageSize, status, categoryId);
    }

    @GetMapping(value = "/retailer/products/{productId}")
    public ProductDTO.ProductResponseDTO getProductByIdAndRetailer(
            Authentication authentication,
            @PathVariable String productId) {
        return productService.getProductByIdAndRetailer(authentication.getName(), productId);
    }

    @GetMapping(value = "/products/{productId}")
    public ProductDTO.ProductResponseDTO getProductById(@PathVariable String productId) {
        return productService.getProductById(productId);
    }

    @PostMapping("/products")
    public ProductDTO.ProductResponseDTO createProduct(
            Authentication authentication,
            @RequestBody @Valid ProductDTO.ProductCreateRequestDTO productCreateRequestDTO) {
        return productService.create(authentication.getName(), productCreateRequestDTO);
    }

    @DeleteMapping(value = "/admin/products/{productId}")
    public ResponseEntity<?> deleteProductByAdmin(@PathVariable String productId) {
        productService.deleteProductByAdmin(productId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/retailer/products/{productId}")
    public ResponseEntity<?> deleteProductByRetailer(
            Authentication authentication,
            @PathVariable String productId) {
        productService.deleteProductByRetailer(authentication.getName(), productId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/products/is-existed-by-slug")
    public boolean isExistedBySlug(@RequestParam String slug) {
        return productRepository.existsBySlug(slug);
    }

    @GetMapping(value = "/products/rate/{productId}")
    public List<OrderItemDTO.RateResponseDTO> getRateProductsPage(
            @PathVariable String productId) {
        return productService.getRateProductByIdDetail(productId);
    }
}