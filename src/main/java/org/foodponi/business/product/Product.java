package org.foodponi.business.product;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.foodponi.business.product_category.Category;
import org.foodponi.business.product_detail.ProductDetail;
import org.foodponi.business.user.User;
import org.foodponi.business.entitybase.AbstractEntity;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity(name = "products")
public class Product extends AbstractEntity {

    @Column(name = "name", columnDefinition = "nvarchar(255)")
    private String name;

    @Column(name = "slug", columnDefinition = "nvarchar(255)")
    private String slug;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "status")
    private boolean status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductDetail> productDetails;

    @ManyToMany
    @JoinTable(
            name = "categories_products",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private List<Category> categories;

}
