package org.foodponi.business.product.enums;

import org.foodponi.core.enums.IErrorCode;

public enum ProductErrorCode implements IErrorCode {

    PRODUCT_NOT_FOUND("P01", "Product not found"),
    PRODUCT_EXISTED("P02", "Product existed"),
    PRODUCT_SLUG_EXISTED("P03", "Product existed with slug"),
    PRODUCT_SLUG_NOT_NULL("P06", "Product slug must not be null"),
    PRODUCT_SLUG_NOT_FOUND("P07", "Product slug must not found"),
    NAME_AND_PRODUCTDETAILS_MUST_NOT_BE_NULL("P04", "Name and ProductDetails must not be null"),
    PRODUCT_BLOCKED("P05", "Product blocked");

    ProductErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
