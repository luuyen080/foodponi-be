package org.foodponi.business.product;

import org.foodponi.business.product_category.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface IProductRepository extends JpaRepository<Product, String> {

    boolean existsBySlug(String slug);

    @Query("select e from products e " +
            "join users u on e.user.id = u.id " +
            "where u.id = ?1 and e.status = ?2")
    Page<Product> findAllByUserIdAndStatus(Pageable pageable, String userId, boolean status);

    @Query("select e from products e " +
            "join users u on e.user.id = u.id " +
            "where u.id = ?1")
    Page<Product> findAllByUserId(Pageable pageable, String userId);

    Page<Product> findAllByStatus(Pageable pageable, boolean status);

    Page<Product> findAllByStatusAndCategories(Pageable pageable, boolean status, Category category);

    Optional<Product> findByIdAndUserId(String productId, String userId);
}
