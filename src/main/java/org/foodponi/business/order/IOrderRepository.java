package org.foodponi.business.order;

import org.foodponi.business.order.enums.OrderStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IOrderRepository extends JpaRepository<Order, String> {

    Page<Order> findAllByUserId(Pageable pageable, String userId);

    Page<Order> findAllByUserIdAndStatus(Pageable pageable, String userId, OrderStatus status);

    Optional<Order> findByIdAndUserId(String orderId, String userId);

    @Query("select e " +
            "from orders e " +
            "         join order_items oi on e.id = oi.order.id " +
            "         join product_details pd on oi.productDetail.id = pd.id " +
            "         join products p on pd.product.id = p.id " +
            "where p.user.id = ?1 and e.id = ?2")
    Optional<Order> findByOrderItemsProductDetailProductUserId(String retailerId, String orderId);

    @Query("select e from orders e " +
            "join order_items oi on e.id = oi.order.id " +
            "join product_details pd on oi.productDetail.id = pd.id " +
            "join products p on pd.product.id = p.id where p.user.id = ?1 and e.status = ?2 " +
            "group by e.id")
    Page<Order> findAllByOrderItemsProductDetailProductUserIdAndStatus(Pageable pageable, String retailerId, OrderStatus status);

    @Query("select e from orders e " +
            "join order_items oi on e.id = oi.order.id " +
            "join product_details pd on oi.productDetail.id = pd.id " +
            "join products p on pd.product.id = p.id where p.user.id = ?1 " +
            "group by e.id")
    Page<Order> findAllByOrderItemsProductDetailProductUserId(Pageable pageable, String retailerId);
}
