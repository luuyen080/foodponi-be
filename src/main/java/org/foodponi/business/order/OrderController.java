package org.foodponi.business.order;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.RequiredArgsConstructor;
import org.foodponi.business.order.enums.OrderStatus;
import org.foodponi.business.order_item.OrderItemDTO;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    
    @PostMapping(value = "/orders")
    public OrderDTO.OrderResponseDTO createOrder(
            Authentication authentication,
            @RequestBody @Valid OrderDTO.OrderRequestDTO orderRequestDTO) {

        return orderService.create(authentication.getName(), orderRequestDTO);
    }

    @GetMapping(value = "/customer/orders")
    public Page<OrderDTO.OrderResponseDTO> getOrdersPageByCustomer(
            Authentication authentication,
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize,
            @RequestParam(required = false) OrderStatus orderStatus) {

        return orderService.getOrdersPageByCustomer(authentication.getName(), Utils.getPageable(page, pageSize), orderStatus);
    }

    @GetMapping(value = "/retailer/orders")
    public Page<OrderDTO.OrderResponseDTO> getOrdersPageByRetailer(
            Authentication authentication,
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize,
            @RequestParam(required = false) OrderStatus orderStatus) {

        return orderService.getOrdersPageByRetailer(authentication.getName(), Utils.getPageable(page, pageSize), orderStatus);
    }

    @GetMapping(value = "/retailer/orders/{orderId}")
    public OrderDTO.OrderResponseDTO getOrderByIdByRetailer(
            Authentication authentication,
            @PathVariable String orderId) {

        return orderService.getOrderByIdByRetailer(authentication.getName(), orderId);
    }

    @GetMapping(value = "/customer/orders/{orderId}")
    public OrderDTO.OrderResponseDTO getOrderByIdByCustomer(
            Authentication authentication,
            @PathVariable String orderId) {

        return orderService.getOrderByIdByCustomer(authentication.getName(), orderId);
    }

    @GetMapping(value = "/customer/orders/rate/{orderId}")
    public List<OrderItemDTO.RateResponseDTO> getRateByIdOrderByCustomer(
            Authentication authentication,
            @PathVariable String orderId) {

        return orderService.getRateOrderByIdByCustomer(authentication.getName(), orderId);
    }

    @PatchMapping(value = "/customer/orders/{orderId}")
    public OrderDTO.OrderResponseDTO updateStatusOrderByCustomer(
            Authentication authentication,
            @PathVariable String orderId,
            @RequestParam @Pattern(regexp = "CANCELLED") String orderStatus) {

        return orderService.updateOrderStatusByCustomer(authentication.getName(), orderId, OrderStatus.valueOf(orderStatus));
    }

    @PatchMapping(value = "/retailer/orders/{orderId}")
    public OrderDTO.OrderResponseDTO updateStatusOrderByRetailer(
            Authentication authentication,
            @PathVariable String orderId,
            @RequestParam @Pattern(regexp = "APPROVED|REJECTED") String orderStatus) {

        return orderService.updateOrderStatusByRetailer(authentication.getName(), orderId, OrderStatus.valueOf(orderStatus));
    }

}
