package org.foodponi.business.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.foodponi.business.order.enums.OrderStatus;
import org.foodponi.business.order_item.OrderItemDTO;
import org.foodponi.business.payment.PaymentInfo;
import org.foodponi.business.user.UserDTO;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public abstract class OrderDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class OrderRequestDTO {

        @NotEmpty
        @Valid
        private List<OrderItemDTO.OrderItemRequestDTO> orderItems;

        @NotNull
        @Valid
        private ShippingAddress shippingAddress;

        private String note;

        @JsonIgnore
        @Builder.Default
        private OrderStatus orderStatus = OrderStatus.PENDING;

        @NotNull
        @Valid
        private PaymentInfo payment;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class OrderIdRequestDTO {

        @NotBlank
        private Long id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class OrderStatusRequestDTO {

        private OrderStatus status;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class OrderResponseDTO {

        private String id;

        private Long totalAmount;

        private UserDTO.UserResponseDTO user;

        @JsonIgnoreProperties(value = "order")
        private List<OrderItemDTO.OrderItemResponseDTO> orderItems;

        private ShippingAddress shippingAddress;

        private OrderStatus status;

        private String note;

        private PaymentInfo payment;

        private Timestamp createdDate;

    }

}