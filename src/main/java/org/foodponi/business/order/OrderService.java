package org.foodponi.business.order;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.order.enums.OrderErrorCode;
import org.foodponi.business.order.enums.OrderStatus;
import org.foodponi.business.order_item.IOrderItemRepository;
import org.foodponi.business.order_item.OrderItem;
import org.foodponi.business.order_item.OrderItemDTO;
import org.foodponi.business.order_item.enums.OrderItemErrorCode;
import org.foodponi.business.payment.PaymentConfig;
import org.foodponi.business.product.IProductRepository;
import org.foodponi.business.product.enums.ProductErrorCode;
import org.foodponi.business.product_detail.IProductDetailRepository;
import org.foodponi.business.product_detail.ProductDetail;
import org.foodponi.business.product_detail.enums.ProductDetailErrorCode;
import org.foodponi.business.user.IUserRepository;
import org.foodponi.business.user.User;
import org.foodponi.business.user.enums.Role;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.UserPayload;
import org.foodponi.core.security.UserContext;
import org.foodponi.core.util.MapperUtils;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final IOrderRepository orderRepository;

    private final IOrderItemRepository orderItemRepository;

    private final IProductRepository productRepository;

    private final IProductDetailRepository productDetailRepository;

    private final IUserRepository userRepository;

    private final Environment environment;

    private final HttpServletRequest httpServletRequest;

    private final PaymentConfig paymentConfig;

    private final UserContext userContext;

    /**
     * @param userId
     * @param orderRequestDTO
     * @return
     */
    @SneakyThrows
    public OrderDTO.OrderResponseDTO create(String userId, OrderDTO.OrderRequestDTO orderRequestDTO) {
        OrderDTO.OrderResponseDTO orderResponseDTO;
        try {
            log.info("OrderService::create new order execution started...");
            User user = userRepository.findById(userId).orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

            // check duplicate product in the order
            long countDistinct = orderRequestDTO.getOrderItems()
                    .stream()
                    .map(orderItem -> {
                        if (orderItem == null)
                            throw new FoodException(OrderItemErrorCode.ORDER_ITEM_CONTAINS_NULL_OBJECT);
                        return orderItem.getProductDetail().getId();
                    })
                    .distinct()
                    .count();
            if (countDistinct != orderRequestDTO.getOrderItems().size())
                throw new FoodException(OrderItemErrorCode.ORDER_ITEM_DUPLICATED);

            // calculate total amount
            Set<String> storeIds = new HashSet<>();
            long totalAmount = orderRequestDTO.getOrderItems().stream().mapToLong(orderItemDTO -> {
                // check product detail not found
                ProductDetail productDetail = productDetailRepository.findById(orderItemDTO.getProductDetail().getId())
                        .orElseThrow(() -> new FoodException(ProductDetailErrorCode.PRODUCT_DETAIL_NOT_FOUND));

                // check status product detail
                if (!productDetail.isStatus()) throw new FoodException(ProductDetailErrorCode.PRODUCT_DETAIL_BLOCKED);

                // check status product
                if (!productDetail.getProduct().isStatus()) throw new FoodException(ProductErrorCode.PRODUCT_BLOCKED);

                // add to the set to check product in the same store
                storeIds.add(productDetail.getProduct().getUser().getId());

                // set price of order item
                orderItemDTO.setPrice(productDetail.getPrice());
                return orderItemDTO.getPrice() * orderItemDTO.getQuantity();
            }).sum();

            //check products must be from the same retailer
            if (storeIds.size() != 1)
                throw new FoodException(OrderItemErrorCode.ORDER_ITEM_NOT_SAME_RETAILER);

            //check customer and retailer must be different
            String retailer = storeIds.iterator().next();
            if (userId.equals(retailer)) throw new FoodException(OrderErrorCode.ORDER_CUSTOMER_RETAILER_SAME);

            // create order
            Order order = MapperUtils.map(orderRequestDTO, Order.class);

            // set total amount
            order.setTotalAmount(totalAmount);

            // set customer
            order.setUser(user);
            order.setStatus(OrderStatus.PENDING);
            //save order
            Order orderResult = orderRepository.save(order);

            // update order items
            orderResult.getOrderItems().stream().forEach(orderItem -> {
                // set order id to the order item again
                orderItem.setOrder(orderResult);
                orderItemRepository.save(orderItem);
            });

            orderResponseDTO = MapperUtils.map(orderResult, OrderDTO.OrderResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting a new order to database , Exception message {}", ex.getErrorMessage());
            throw ex;
        }

        log.info("OrderService::create execution ended...");
        return orderResponseDTO;
    }

    /**
     * @param userId
     * @param orderId
     * @return
     */
    public OrderDTO.OrderResponseDTO getOrderByIdByRetailer(String userId, String orderId) {
        Order orderResponse;
        try {
            log.info("OrderService::getOrderByIdByRetailer execution started...");
            orderResponse = orderRepository.findByOrderItemsProductDetailProductUserId(userId, orderId).orElseThrow(() -> new FoodException(OrderErrorCode.ORDER_NOT_FOUND));

            log.info("OrderService::getOrderByIdByRetailer execution ended...");
            return MapperUtils.map(orderResponse, OrderDTO.OrderResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving order {} from database , Exception message {}", orderId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param orderId
     * @return
     */
    public OrderDTO.OrderResponseDTO getOrderByIdByCustomer(String userId, String orderId) {
        Order orderResponse;
        try {
            log.info("OrderService::getOrderByIdByCustomer execution started...");
            orderResponse = orderRepository.findByIdAndUserId(orderId, userId).orElseThrow(() -> new FoodException(OrderErrorCode.ORDER_NOT_FOUND));

            log.info("OrderService::getOrderByIdByCustomer execution ended...");
            return MapperUtils.map(orderResponse, OrderDTO.OrderResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving order {} from database , Exception message {}", orderId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param orderId
     * @param orderStatus
     * @return
     */
    public OrderDTO.OrderResponseDTO updateOrderStatusByCustomer(String userId, String orderId, OrderStatus orderStatus) {
        OrderDTO.OrderResponseDTO orderResponseDTO;
        try {
            log.info("OrderService::updateOrderStatusByCustomer execution started...");

            //CHECK EXISTED
            Order existOrder = orderRepository.findByIdAndUserId(orderId, userId).orElseThrow(() -> new FoodException(OrderErrorCode.ORDER_NOT_FOUND));

            //EXECUTE
            existOrder.setStatus(orderStatus);
            Order orderResult = orderRepository.save(existOrder);

            orderResponseDTO = MapperUtils.map(orderResult, OrderDTO.OrderResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting order to database, Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("OrderService::updateOrderStatusByCustomer execution ended...");
        return orderResponseDTO;
    }

    /**
     *
     * @param userId
     * @param orderId
     * @param orderStatus
     * @return
     */
    public OrderDTO.OrderResponseDTO updateOrderStatusByRetailer(String userId, String orderId, OrderStatus orderStatus) {
        OrderDTO.OrderResponseDTO orderResponseDTO;
        try {
            log.info("OrderService::updateOrderStatusById execution started...");

            //CHECK EXISTED
            Order existOrder = orderRepository.findById(orderId).orElseThrow(() -> new FoodException(OrderErrorCode.ORDER_NOT_FOUND));

            //check is retailer and status is approved
            if (!(orderStatus.equals(OrderStatus.APPROVED)))
                throw new FoodException(HttpStatus.FORBIDDEN, CommonErrorCode.ACCESS_FORBIDDEN);

            //check retailer in order is exactly
            OrderItem orderItem = existOrder.getOrderItems().stream().findFirst().orElseThrow(() -> new FoodException(ProductErrorCode.PRODUCT_NOT_FOUND));
            User retailer = orderItem.getProductDetail().getProduct().getUser();
            if (!userId.equals(retailer.getId()))
                throw new FoodException(HttpStatus.FORBIDDEN, CommonErrorCode.ACCESS_FORBIDDEN);

            //EXECUTE
            existOrder.setStatus(orderStatus);
            Order orderResult = orderRepository.save(existOrder);

            orderResponseDTO = MapperUtils.map(orderResult, OrderDTO.OrderResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting order to database, Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("OrderService::updateOrderStatusById execution ended...");
        return orderResponseDTO;
    }

    /**
     * @param userId
     * @param pageable
     * @param status
     * @return
     */
    public Page<OrderDTO.OrderResponseDTO> getOrdersPageByCustomer(String userId, Pageable pageable, OrderStatus status) {
        Page<Order> ordersResponse;
        try {
            log.info("OrderService::getOrdersPageByCustomer execution started...");
            ordersResponse = status != null
                    ? orderRepository.findAllByUserIdAndStatus(pageable, userId, status)
                    : orderRepository.findAllByUserId(pageable, userId);

        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }
        log.info("OrderService::getOrdersPageByCustomer execution ended...");
        Page<OrderDTO.OrderResponseDTO> dto = MapperUtils.mapPage(ordersResponse, OrderDTO.OrderResponseDTO.class);
        return dto;
    }

    public Page<OrderDTO.OrderResponseDTO> getOrdersPageByRetailer(String userId, Pageable pageable, OrderStatus status) {
        Page<Order> ordersResponse;
        try {
            log.info("OrderService::getOrdersPageByRetailer execution started...");

            ordersResponse = status != null
                    ? orderRepository.findAllByOrderItemsProductDetailProductUserIdAndStatus(pageable, userId, status)
                    : orderRepository.findAllByOrderItemsProductDetailProductUserId(pageable, userId);

        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("OrderService::getOrdersPageByRetailer execution ended...");
        return MapperUtils.mapPage(ordersResponse, OrderDTO.OrderResponseDTO.class);
    }

    /**
     * @param name
     * @param orderId
     * @return
     */
    public List<OrderItemDTO.RateResponseDTO> getRateOrderByIdByCustomer(String name, String orderId) {
        List<OrderItemDTO.RateResponseDTO> rateResponse = new ArrayList<>();
        try {
            log.info("OrderService::getOrdersPageByRetailer execution started...");

            Order existOrder = orderRepository.findByIdAndUserId(orderId, name).orElseThrow(() -> new FoodException(OrderErrorCode.ORDER_NOT_FOUND));
            for (OrderItem orderItem : existOrder.getOrderItems()) {
                if (orderItem.getRate() != null) {
                    OrderItemDTO.RateResponseDTO rate = new OrderItemDTO.RateResponseDTO().builder()
                            .rate(orderItem.getRate().getRate())
                            .images(orderItem.getRate().getImages())
                            .message(orderItem.getRate().getMessage())
                            .name(orderItem.getProductDetail().getProduct().getName())
                            .thumbnail(orderItem.getProductDetail().getProduct().getThumbnail())
                            .build();
                    rateResponse.add(rate);
                }
            }
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }
        log.info("OrderService::getOrdersPageByRetailer execution ended...");
        return rateResponse;
    }
}
