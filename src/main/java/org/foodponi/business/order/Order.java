package org.foodponi.business.order;

import com.vladmihalcea.hibernate.type.json.JsonType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.foodponi.business.entitybase.AbstractEntity;
import org.foodponi.business.order.enums.OrderStatus;
import org.foodponi.business.order_item.OrderItem;
import org.foodponi.business.payment.PaymentInfo;
import org.foodponi.business.user.User;
import org.hibernate.annotations.Type;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "orders")
@SuperBuilder
public class Order extends AbstractEntity {

    @Column(name = "total_amount")
    private Long totalAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OrderStatus status;

    @Type(JsonType.class)
    @Column(name = "shipping_address", columnDefinition = "json")
    private ShippingAddress shippingAddress;

    @Column(name = "note")
    private String note;

    @Type(JsonType.class)
    @Column(name = "payment", columnDefinition = "json")
    private PaymentInfo payment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;

}
