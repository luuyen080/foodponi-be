package org.foodponi.business.order.enums;

public enum OrderStatus {

    PENDING,
    APPROVED,
    CANCELLED,
    REJECTED,
    COMPLETED

}
