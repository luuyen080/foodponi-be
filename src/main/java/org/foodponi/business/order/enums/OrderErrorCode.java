package org.foodponi.business.order.enums;

import org.foodponi.core.enums.IErrorCode;

public enum OrderErrorCode implements IErrorCode {

    INVALID_ORDER("O01", "Invalid Order"),

    ORDER_NOT_FOUND("O02", "Order not found"),

    ORDER_CUSTOMER_RETAILER_SAME("O03", "Customer and retailer are same"),

    ORDER_MUST_BE_COMPLETED("O04", "Order must be completed");

    OrderErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
