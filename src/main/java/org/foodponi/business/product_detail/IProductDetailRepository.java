package org.foodponi.business.product_detail;

import org.foodponi.business.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IProductDetailRepository extends JpaRepository<ProductDetail, String> {

}
