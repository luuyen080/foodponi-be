package org.foodponi.business.product_detail.enums;

import org.foodponi.core.enums.IErrorCode;

public enum ProductDetailErrorCode implements IErrorCode {

    PRODUCT_DETAIL_NOT_FOUND("PD01", "Product detail not found"),
    PRODUCT_DETAIL_EXISTED("PD02", "Product detail existed"),
    PRODUCT_DETAIL_EMPTY("PD03", "Product detail is empty"),
    PRODUCT_DETAIL_NAME_DUPLICATED("PD04", "Product detail's name is duplicated"),
    PRODUCT_DETAIL_BLOCKED("PD05", "Product detail blocked"),;

    ProductDetailErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
