package org.foodponi.business.product_detail;

import com.vladmihalcea.hibernate.type.json.JsonType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.foodponi.business.order_item.OrderItem;
import org.foodponi.business.product.Product;
import org.foodponi.business.entitybase.AbstractEntity;
import org.hibernate.annotations.Type;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity(name = "product_details")
public class ProductDetail extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Long price;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Type(JsonType.class)
    @Column(name = "images", columnDefinition = "json")
    private List<String> images;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "status")
    private boolean status;

    @OneToMany(mappedBy = "productDetail")
    private List<OrderItem> orderItems;

}
