package org.foodponi.business.product_detail;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.*;
import lombok.*;
import org.foodponi.business.product.ProductDTO;

import java.util.List;

public abstract class ProductDetailDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ProductDetailCreateRequestDTO {

        private String name;

        @Min(value = 0)
        private long price;

        private String description;

        private List<String> images;

        @JsonIgnore
        @Builder.Default
        private boolean status = true;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ProductDetailUpdateRequestDTO {

        @Size(min = 36, max = 36)
        private String id;

        private String name;

        @Min(value = 0)
        private long price;

        private String description;

        private List<String> images;

    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class ProductDetailIdDTO {

        @NotBlank
        private String id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ProductDetailResponseDTO {

        private String id;

        private String name;

        private Long price;

        private String description;

        private boolean status;

        private List<String> images;

        @JsonIgnoreProperties(value = "productDetails")
        private ProductDTO.ProductResponseDTO product;

        private double rate;

        private int sales;

        private int rateCount;

    }

}

