package org.foodponi.business.order_item.enums;

import org.foodponi.core.enums.IErrorCode;

public enum OrderItemErrorCode implements IErrorCode {

    ORDER_ITEM_NOT_SAME_RETAILER("OI01", "Order item is not from the same retailer"),

    ORDER_ITEM_DUPLICATED("OI02", "Order item is duplicated"),

    ORDER_ITEM_CONTAINS_NULL_OBJECT("OI03", "Order item must not contain null object"),

    ORDER_ITEM_NOT_FOUND("OI04", "Order item not found"),

    ORDER_ITEM_ALREADY_RATED("OI05", "Order item already rated");

    OrderItemErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
