package org.foodponi.business.order_item;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/order-items")
@RequiredArgsConstructor
public class OrderItemController {

    private final OrderItemService orderItemService;

    @PostMapping("/rate/{orderItemId}")
    public OrderItemDTO.OrderItemResponseDTO createRate(
            Authentication authentication,
            @PathVariable String orderItemId,
            @RequestBody @Valid Rate rate) {
        return orderItemService.createRate(authentication.getName(), orderItemId, rate);
    }

}
