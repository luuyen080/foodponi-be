package org.foodponi.business.order_item;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.order.enums.OrderErrorCode;
import org.foodponi.business.order.enums.OrderStatus;
import org.foodponi.business.order_item.enums.OrderItemErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.util.MapperUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class OrderItemService {

    private final IOrderItemRepository orderItemRepository;

    /**
     * @param userId
     * @param orderItemId
     * @param rate
     * @return
     */
    public OrderItemDTO.OrderItemResponseDTO createRate(String userId, String orderItemId, Rate rate) {
        try {
            log.info("createRate::create execution started...");

            OrderItem orderItem = orderItemRepository.findById(orderItemId).orElseThrow(() -> new FoodException(OrderItemErrorCode.ORDER_ITEM_NOT_FOUND));

            // check user in order
            if (!orderItem.getOrder().getUser().getId().equals(userId)) {
                throw new FoodException(HttpStatus.FORBIDDEN, CommonErrorCode.ACCESS_FORBIDDEN);
            }

            // check order status
            if (!orderItem.getOrder().getStatus().equals(OrderStatus.COMPLETED)) {
                throw new FoodException(OrderErrorCode.ORDER_MUST_BE_COMPLETED);
            }

            // check order item is rated
            if (orderItem.getRate() != null) {
                throw new FoodException(OrderItemErrorCode.ORDER_ITEM_ALREADY_RATED);
            }
            orderItem.setRate(rate);
            return MapperUtils.map(orderItemRepository.save(orderItem), OrderItemDTO.OrderItemResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting product to database , Exception message {}", ex.getMessage());
            throw ex;
        }
    }

}
