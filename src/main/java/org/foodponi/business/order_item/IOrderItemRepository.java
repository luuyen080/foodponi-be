package org.foodponi.business.order_item;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrderItemRepository extends JpaRepository<OrderItem, String> {

}
