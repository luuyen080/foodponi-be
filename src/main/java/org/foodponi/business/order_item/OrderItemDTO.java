package org.foodponi.business.order_item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.foodponi.business.order.OrderDTO;
import org.foodponi.business.product_detail.ProductDetailDTO;

import java.util.List;

public abstract class OrderItemDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class OrderItemRequestDTO {

        @Min(value = 1)
        private Integer quantity;

        @JsonIgnore
        private Long price;

        @NotNull
        @Valid
        private ProductDetailDTO.ProductDetailIdDTO productDetail;

        private String note;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class OrderItemIdDTO {

        @NotBlank
        private String id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @ToString
    public static class RateResponseDTO {

        private Integer rate;
        private String message;
        private List<String> images;
        private String name;
        private String thumbnail;
        private String username;
        private String avatar;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @ToString
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class OrderItemResponseDTO {

        private String id;

        private Integer quantity;

        private Long price;

        private ProductDetailDTO.ProductDetailResponseDTO productDetail;

        private String note;

        @JsonIgnoreProperties(value = "orderItems")
        private OrderDTO.OrderResponseDTO order;

        @JsonIgnoreProperties(value = "rate")
        private RateResponseDTO rate;

    }

}