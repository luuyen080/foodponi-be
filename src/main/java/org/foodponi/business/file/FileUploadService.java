package org.foodponi.business.file;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.file.enums.FileUploadErrorCode;
import org.foodponi.business.user.User;
import org.foodponi.business.user.UserDTO;
import org.foodponi.business.user.enums.Role;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.UserPayload;
import org.foodponi.core.security.UserContext;
import org.foodponi.core.security.UserContextDetails;
import org.foodponi.core.util.MapperUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class FileUploadService {

    private final IFileUploadRepository fileUploadRepository;

    @Value("${application.upload-dir}")
    private String UPLOAD_DIR;

    @Value("${spring.mvc.static-path-pattern}")
    private String PATH;

    /**
     * @param userId
     * @param multipartFile
     * @return
     * @throws IOException
     */
    public FileUploadDTO.FileUploadResponseDTO uploadSingleFile(String userId, MultipartFile multipartFile) throws IOException {
        FileUploadDTO.FileUploadResponseDTO fileUploadResponseDTO;
        try {
            log.info("FileUploadService::upload single file execution started...");

            //CHECK FILE NAME
            String originalFileName = multipartFile.getOriginalFilename();
            String fileName = createFileName(originalFileName.substring(0, originalFileName.lastIndexOf('.')));
            String extension = originalFileName.substring(originalFileName.lastIndexOf('.'));
            String fileNameWithExtension = fileName + extension;

            //SET FILE_UPLOAD
            FileUpload fileUpload = new FileUpload();
            fileUpload.setName(fileName);
            fileUpload.setExtension(extension);
            fileUpload.setContentType(multipartFile.getContentType());
            fileUpload.setSize(multipartFile.getSize());
            fileUpload.setUrl(
                    ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString()
                            + PATH.replace("**", "") + fileNameWithExtension);
            if (multipartFile.getContentType().startsWith("image/")) {
                BufferedImage bufferedImage = ImageIO.read(multipartFile.getInputStream());
                fileUpload.setDimension(new Dimension(bufferedImage.getWidth(), bufferedImage.getHeight()));
            }
            User user = new User();
            user.setId(userId);
            fileUpload.setUser(user);

            //CHECK DIRECTORY EXISTED
            File directory = new File(System.getProperty("user.dir") + UPLOAD_DIR);
            if (!directory.exists())
                directory.mkdirs();

            //SAVE FILE TO DIRECTORY
            multipartFile.transferTo(new File(directory, fileNameWithExtension));

            //SAVE FILE_UPLOAD TO DATABASE
            FileUpload fileResult = fileUploadRepository.save(fileUpload);
            fileUploadResponseDTO = MapperUtils.map(fileResult, FileUploadDTO.FileUploadResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting a new fileUpload to database , Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("FileUploadService::create execution ended...");
        return fileUploadResponseDTO;
    }

    /**
     * @param userId
     * @return
     */
    public Page<FileUploadDTO.FileUploadResponseDTO> getFileUploadsPage(String userId, Pageable pageable) {
        Page<FileUpload> fileUploadsResponse;
        try {
            log.info("UserService::getFileUploadsPage execution started...");

            fileUploadsResponse = fileUploadRepository.findAllByUserId(pageable, userId);

        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("UserService::getFileUploadsPage execution ended...");
        return MapperUtils.mapPage(fileUploadsResponse, FileUploadDTO.FileUploadResponseDTO.class);
    }

    /**
     * @param fileUploadId
     * @return
     */
    public FileUploadDTO.FileUploadResponseDTO getFileUploadById(String userId, String fileUploadId) {
        FileUploadDTO.FileUploadResponseDTO fileUploadResponseDTO;
        try {
            log.info("FileUploadService::getFileUploadById execution started...");

            FileUpload fileUpload = fileUploadRepository.findById(fileUploadId).orElseThrow(() -> new FoodException(FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND));

            if (!userId.equals(fileUpload.getUser().getId())) {
                throw new FoodException(CommonErrorCode.ACCESS_FORBIDDEN);
            }
            fileUploadResponseDTO = MapperUtils.map(fileUpload, FileUploadDTO.FileUploadResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving fileUpload {} from database , Exception message {}", fileUploadId, ex.getMessage());
            throw ex;
        }

        log.info("FileUploadService::getFileUploadById execution ended...");
        return fileUploadResponseDTO;
    }

    /**
     * @param fileUploadId
     * @param fileName
     * @return
     */
    public FileUploadDTO.FileUploadResponseDTO updateNameFileUploadById(String userId, String fileUploadId, FileUploadDTO.NameFileUploadRequest fileName) {
        FileUploadDTO.FileUploadResponseDTO fileUploadResponseDTO;
        try {
            log.info("FileUploadService::updateNameFileUploadById execution started...");

            //CHECK EXISTED
            FileUpload existFileUpload = fileUploadRepository.findById(fileUploadId).orElseThrow(() -> new FoodException(FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND));

            if (!userId.equals(existFileUpload.getUser().getId())) {
                throw new FoodException(CommonErrorCode.ACCESS_FORBIDDEN);
            }

            //EXECUTE
            if (existFileUpload.getName().equals(fileName.getName()))
                fileUploadResponseDTO = MapperUtils.map(existFileUpload, FileUploadDTO.FileUploadResponseDTO.class);
            else {
                //get old file name
                String oldFileNameWithExtension = existFileUpload.getName() + existFileUpload.getExtension();

                //check existed file name
                String newFileName = createFileName(fileName.getName());
                String newFileNameWithExtension = newFileName + existFileUpload.getExtension();

                //update new file name in database
                existFileUpload.setName(newFileName);
                existFileUpload.setUrl(
                        ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString()
                                + PATH.replace("**", "") + newFileNameWithExtension);
                FileUpload fileUploadResult = fileUploadRepository.save(existFileUpload);

                //update file name in directory
                File oldFile = new File(System.getProperty("user.dir") + UPLOAD_DIR + "/" + oldFileNameWithExtension);
                File newFile = new File(System.getProperty("user.dir") + UPLOAD_DIR + "/" + newFileNameWithExtension);
                if (!oldFile.renameTo(newFile))
                    log.error("Update file name failed");

                fileUploadResponseDTO = MapperUtils.map(fileUploadResult, FileUploadDTO.FileUploadResponseDTO.class);
            }
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting fileUpload to database, Exception message {}", ex.getMessage());
            throw ex;
        }

        log.info("FileUploadService::updateNameFileUploadById execution ended...");
        return fileUploadResponseDTO;
    }

    /**
     * @param fileUploadId
     */
    public void deleteFileUploadById(String userId, String fileUploadId) {
        try {
            log.info("FileUploadService::deleteFileUploadById execution started...");

            //CHECK EXIST
            FileUpload existFileUpload = fileUploadRepository.findById(fileUploadId).orElseThrow(() -> new FoodException(FileUploadErrorCode.FILE_UPLOAD_NOT_FOUND));

            if (!userId.equals(existFileUpload.getUser().getId())) {
                throw new FoodException(CommonErrorCode.ACCESS_FORBIDDEN);
            }

            //EXECUTE
            fileUploadRepository.delete(existFileUpload);
            String fileNameWithExtension = existFileUpload.getName() + existFileUpload.getExtension();
            File file = new File(System.getProperty("user.dir") + UPLOAD_DIR + "/" + fileNameWithExtension);
            file.delete();
        } catch (FoodException ex) {
            log.error("Exception occurred while deleting fileUpload {} from database, Exception message {}", fileUploadId, ex.getMessage());
            throw ex;
        }

        log.info("FileUploadService::deleteFileUploadById execution ended...");
    }

    /**
     * @param fileName
     * @return
     */
    private String createFileName(String fileName) {
        int i = 1;
        StringBuilder newFileName = new StringBuilder(fileName);
        while (fileUploadRepository.existsByName(newFileName.toString())) {
            newFileName.setLength(0);
            newFileName.append(fileName).append("-").append(i++);
        }
        return newFileName.toString();
    }

}