package org.foodponi.business.file;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/file-uploads")
@RequiredArgsConstructor
public class FileUploadController {

    private final FileUploadService fileUploadService;

    @PostMapping(value = "")
    public FileUploadDTO.FileUploadResponseDTO uploadSingleFile(
            Authentication authentication,
            @RequestParam("multipartFile") MultipartFile multipartFile) throws IOException {

        return fileUploadService.uploadSingleFile(authentication.getName(), multipartFile);
    }

    @GetMapping
    public Page<FileUploadDTO.FileUploadResponseDTO> getFileUploads(
            Authentication authentication,
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize) {

        return fileUploadService.getFileUploadsPage(authentication.getName(), Utils.getPageable(page, pageSize));
    }

    @GetMapping(value = "/{fileUploadId}")
    public FileUploadDTO.FileUploadResponseDTO getFileUploadById(
            Authentication authentication,
            @PathVariable String fileUploadId) {

        return fileUploadService.getFileUploadById(authentication.getName(), fileUploadId);
    }

    @PatchMapping(value = "/{fileUploadId}")
    public FileUploadDTO.FileUploadResponseDTO updateNameFileUploadById(
            Authentication authentication,
            @PathVariable String fileUploadId,
            @RequestBody @Valid FileUploadDTO.NameFileUploadRequest fileName) {

        return fileUploadService.updateNameFileUploadById(authentication.getName(), fileUploadId, fileName);
    }

    @DeleteMapping(value = "/{fileUploadId}")
    public ResponseEntity<?> deleteFileUploadById(
            Authentication authentication,
            @PathVariable String fileUploadId) {

        fileUploadService.deleteFileUploadById(authentication.getName(), fileUploadId);
        return ResponseEntity.noContent().build();
    }

}
