package org.foodponi.business.file;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFileUploadRepository extends JpaRepository<FileUpload, String> {

    boolean existsByName(String name);

    Page<FileUpload> findAllByUserId(Pageable pageable, String userId);

}
