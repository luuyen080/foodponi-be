package org.foodponi.business.file;

import jakarta.persistence.*;
import lombok.*;
import org.foodponi.business.entitybase.AbstractEntity;
import org.foodponi.business.user.User;

import java.awt.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "file_uploads")
public class FileUpload extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "extension")
    private String extension;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "size")
    private Long size;

    @Column(name = "url")
    private String url;

    @Column(name = "dimension")
    private Dimension dimension;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

}
