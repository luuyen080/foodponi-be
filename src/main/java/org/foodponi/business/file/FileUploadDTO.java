package org.foodponi.business.file;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.foodponi.business.user.User;
import org.foodponi.business.user.UserDTO;

import java.awt.*;

public abstract class FileUploadDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NameFileUploadRequest {

        @NotBlank
        private String name;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class FileUploadResponseDTO {

        private String id;

        private String name;

        private String extension;

        private String contentType;

        private Long size;

        private String url;

        private Dimension dimension;

        private UserDTO.UserResponseDTO user;

    }
}

