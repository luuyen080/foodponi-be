package org.foodponi.business.file.enums;

import org.foodponi.core.enums.IErrorCode;

public enum FileUploadErrorCode implements IErrorCode {

    FILE_UPLOAD_NOT_FOUND("FU01", "FileUpload not found"),
    FILE_UPLOAD_EXISTED("FU02", "FileUpload existed");

    FileUploadErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
