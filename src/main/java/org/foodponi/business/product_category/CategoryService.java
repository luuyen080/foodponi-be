package org.foodponi.business.product_category;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.product_category.enums.CategoryErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.util.MapperUtils;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryService {

    private final ICategoryRepository categoryRepository;

    /**
     * @param onlyParent
     * @param page
     * @param pageSize
     * @return
     */
    public Page<CategoryDTO.CategoryResponseDTO> getCategoriesPage(boolean onlyParent, Integer page, Integer pageSize) {
        Page<Category> categoriesResponse;
        try {
            log.info("ProductCategoryService::getCategoriesPage execution started...");

            categoriesResponse = onlyParent ?
                    categoryRepository.findAllByParentCategoryIsNull(Utils.getPageable(page, pageSize)) :
                    categoryRepository.findAll(Utils.getPageable(page, pageSize));

        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }
        log.info("ProductCategoryService::getCategoriesPage execution ended...");
        return MapperUtils.mapPage(categoriesResponse, CategoryDTO.CategoryResponseDTO.class);
    }

    /**
     * @param id
     * @return
     */
    public CategoryDTO.CategoryResponseDTO getCategoryById(String id) {
        log.info("getCategoryById started");

        Optional<Category> categoryOptional = categoryRepository.findById(id);

        Category category = categoryOptional.orElseThrow(() -> new FoodException(CategoryErrorCode.CATEGORY_NOT_FOUND));

        log.info("getCategoryById end");

        return MapperUtils.map(category, CategoryDTO.CategoryResponseDTO.class);
    }

    /**
     * @param categoryRequestDTO
     * @return
     */
    public CategoryDTO.CategoryResponseDTO create(CategoryDTO.CategoryRequestDTO categoryRequestDTO) {
        log.info("create category started");
        if (categoryRepository.existsByCategoryName(categoryRequestDTO.getCategoryName())) {
            throw new FoodException(CategoryErrorCode.CATEGORY_EXISTED);
        }

        Category category = categoryRepository.save(MapperUtils.map(categoryRequestDTO, Category.class));

        log.info("create category end");

        return MapperUtils.map(category, CategoryDTO.CategoryResponseDTO.class);
    }

}
