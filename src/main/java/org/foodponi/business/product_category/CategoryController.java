package org.foodponi.business.product_category;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/product-categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public Page<CategoryDTO.CategoryResponseDTO> getCategoriesPage(
            @RequestParam(required = false) boolean onlyParent,
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize) {

        return categoryService.getCategoriesPage(onlyParent, page, pageSize);
    }

    @GetMapping(value = "/{id}")
    public CategoryDTO.CategoryResponseDTO getCategoryById(
            @PathVariable String id) {

        return categoryService.getCategoryById(id);
    }

    @PostMapping
    public CategoryDTO.CategoryResponseDTO createCategory(
            @RequestBody @Valid CategoryDTO.CategoryRequestDTO categoryRequestDTO) {

        return categoryService.create(categoryRequestDTO);
    }

}
