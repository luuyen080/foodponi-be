package org.foodponi.business.product_category;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.util.List;

public abstract class CategoryDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CategoryRequestDTO {

        @NotBlank
        private String categoryName;

        private String description;

        private String thumbnail;

        private CategoryIdDTO parentCategory;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CategoryIdDTO {

        @NotBlank
        private String id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CategoryResponseDTO {

        private String id;

        private String categoryName;

        private String slug;

        private String description;

        private String thumbnail;

        @JsonIgnoreProperties(value = "parentCategory")
        private List<CategoryResponseDTO> categories;

        @JsonIgnoreProperties(value = "categories")
        private CategoryResponseDTO parentCategory;

    }

}
