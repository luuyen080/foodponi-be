package org.foodponi.business.product_category.enums;

import org.foodponi.core.enums.IErrorCode;

public enum CategoryErrorCode implements IErrorCode {

    CATEGORY_NOT_FOUND("C01", "Category not found"),
    CATEGORY_NAME_MUST_NOT_BE_NULL("C03", "Category name must not be null"),
    CATEGORY_EXISTED("C02", "Category existed");

    CategoryErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
