package org.foodponi.business.product_category;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ICategoryRepository extends JpaRepository<Category, String> {

    Page<Category> findAllByParentCategoryIsNull(Pageable pageable);

    boolean existsByCategoryName(String categoryName);

}
