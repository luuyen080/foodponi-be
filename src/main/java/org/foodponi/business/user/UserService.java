package org.foodponi.business.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.foodponi.business.address.Address;
import org.foodponi.business.address.AddressDTO;
import org.foodponi.business.address.IAddressRepository;
import org.foodponi.business.address.enums.AddressErrorCode;
import org.foodponi.business.user.enums.Role;
import org.foodponi.business.user.enums.UserErrorCode;
import org.foodponi.core.enums.CommonErrorCode;
import org.foodponi.core.exception.FoodException;
import org.foodponi.core.model.UserPayload;
import org.foodponi.core.security.UserContext;
import org.foodponi.core.util.MapperUtils;
import org.foodponi.core.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final IUserRepository userRepository;

    private final IAddressRepository addressRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserContext userContext;

    /**
     * @param userCreationRequestDTO
     * @return
     */
    public UserDTO.UserResponseDTO create(UserDTO.UserCreationRequestDTO userCreationRequestDTO) {
        log.info("UserService::create user execution started...");

        //Email and username is null
        if (userCreationRequestDTO.getEmail() == null && userCreationRequestDTO.getUsername() == null)
            throw new FoodException(UserErrorCode.EMAIL_AND_USERNAME_MUST_NOT_BE_NULL);

        //Email existed
        if (userRepository.existsByEmail(userCreationRequestDTO.getEmail()))
            throw new FoodException(UserErrorCode.EMAIL_EXIST);

        //Username existed
        if (userRepository.existsByUsername(userCreationRequestDTO.getUsername()))
            throw new FoodException(UserErrorCode.USERNAME_EXIST);

        // Save the new user to the repository
        userCreationRequestDTO.setPassword(passwordEncoder.encode(userCreationRequestDTO.getPassword()));
        log.info("UserService::create user execution end...");
        return MapperUtils.map(userRepository.save(MapperUtils.map(userCreationRequestDTO, User.class)), UserDTO.UserResponseDTO.class);
    }

    /**
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    public Page<UserDTO.UserResponseDTO> getUsersPageByAdmin(Integer page, Integer pageSize, Boolean status) {
        Page<User> usersResponse;
        try {
            log.info("UserService::getUsersPageByAdmin execution started...");

            usersResponse = status != null
                    ? userRepository.findAllByStatus(Utils.getPageable(page, pageSize), status)
                    : userRepository.findAll(Utils.getPageable(page, pageSize));

            log.info("UserService::getUsersPageByAdmin execution ended...");
            return MapperUtils.mapPage(usersResponse, UserDTO.UserResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    public Page<UserDTO.UserResponseDTO> getUsersPageByRetailer(String userId, Integer page, Integer pageSize, Boolean status) {
        Page<User> usersResponse;
        try {
            log.info("UserService::getUsersPageByRetailer execution started...");

            usersResponse = status != null
                    ? userRepository.findAllByRetailerAndStatus(Utils.getPageable(page, pageSize), userId, status)
                    : userRepository.findAllByRetailer(Utils.getPageable(page, pageSize), userId);

            log.info("UserService::getUsersPageByRetailer execution ended...");
            return MapperUtils.mapPage(usersResponse, UserDTO.UserResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving orders from database , Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @return
     */
    public UserDTO.UserResponseDTO getUserById(String userId) {
        try {
            log.info("UserService::getUserById execution started...");

            User user = userRepository.findById(userId).orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

            log.info("UserService::getUserById execution ended...");
            return MapperUtils.map(user, UserDTO.UserResponseDTO.class);
        } catch (FoodException ex) {
            log.error("Exception occurred while retrieving user {} from database , Exception message {}", userId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param userUpdateAvatarRequestDTO
     * @return
     */
    public UserDTO.UserResponseDTO updateUserAvatar(String userId, UserDTO.UserUpdateAvatarRequestDTO userUpdateAvatarRequestDTO) {
        UserDTO.UserResponseDTO userResponseDTO;
        try {
            log.info("UserService::updateUserAvatar execution started...");

            User user = userRepository.findById(userId)
                    .orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

            user.setAvatar(userUpdateAvatarRequestDTO.getAvatar());

            userResponseDTO = MapperUtils.map(userRepository.save(user), UserDTO.UserResponseDTO.class);

            log.info("UserService::updateUserAvatar execution ended...");
            return userResponseDTO;
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting user to database, Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param addressIdDTO
     * @return
     */
    public UserDTO.UserResponseDTO updateUserAddress(String userId, AddressDTO.AddressIdDTO addressIdDTO) {
        UserDTO.UserResponseDTO userResponseDTO;
        try {
            log.info("UserService::updateUserAddress execution started...");

            User user = userRepository.findById(userId)
                    .orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

            Address address = addressRepository.findById(addressIdDTO.getId())
                    .orElseThrow(() -> new FoodException(AddressErrorCode.ADDRESS_NOT_FOUND));

            if (!address.getUser().getId().equals(userId))
                throw new FoodException(AddressErrorCode.ADDRESS_NOT_FOUND);

            user.setAddress(address);

            User userResult = userRepository.save(user);
            userResponseDTO = MapperUtils.map(userResult, UserDTO.UserResponseDTO.class);

            log.info("UserService::updateUserAddress execution ended...");
            return userResponseDTO;
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting user to database, Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     * @param changePasswordRequestDTO
     */
    public void changePassword(String userId, UserDTO.ChangePasswordRequestDTO changePasswordRequestDTO) {
        log.info("UserService::changePassword execution started...");

        // Check if current user exists
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

        // Check current password
        if (!passwordEncoder.matches(changePasswordRequestDTO.getOldPassword(), user.getPassword())) {
            throw new FoodException(UserErrorCode.CURRENT_PASSWORD_NOT_MATCH);
        }

        // Update password
        user.setPassword(passwordEncoder.encode(changePasswordRequestDTO.getNewPassword()));
        userRepository.save(user);

        log.info("UserService::changePassword execution end...");
    }

    /**
     * @param userUpdateRoleRequestDTO
     */

    public UserDTO.UserResponseDTO updateRoleByAdmin(String adminId, UserDTO.UserUpdateRoleRequestDTO userUpdateRoleRequestDTO) {
        UserDTO.UserResponseDTO userResponseDTO;
        try {
            log.info("UserService::updateRole execution started...");

            if (userUpdateRoleRequestDTO.getId().equals(adminId))
                throw new FoodException(CommonErrorCode.ACCESS_FORBIDDEN);

            // Check if user exists
            User user = userRepository.findById(userUpdateRoleRequestDTO.getId())
                    .orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

            // Update role
            user.setRole(userUpdateRoleRequestDTO.getRole());

            userResponseDTO = MapperUtils.map(userRepository.save(user), UserDTO.UserResponseDTO.class);
            log.info("UserService::updateRole execution end...");
            return userResponseDTO;
        } catch (FoodException ex) {
            log.error("Exception occurred while persisting user to database, Exception message {}", ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param userId
     */
    public void delete(String userId) {
        log.info("UserService::delete execution started...");
        // load user is deleted
        User user = userRepository.findById(userId).orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

        if (user.isStatus()) {
            user.setStatus(false);
            userRepository.save(user);
        } else throw new FoodException(UserErrorCode.USER_NOT_FOUND);

        log.info("UserService::delete execution end...");
    }

    public void deleteByAdmin(String userId, String userIdDelete) {
        log.info("UserService::deleteByAdmin execution started...");
        if (userId.equals(userIdDelete))
            throw new FoodException(HttpStatus.FORBIDDEN, CommonErrorCode.ACCESS_FORBIDDEN);

        // load user is deleted
        User user = userRepository.findById(userIdDelete).orElseThrow(() -> new FoodException(UserErrorCode.USER_NOT_FOUND));

        if (user.isStatus()) {
            user.setStatus(false);
            userRepository.save(user);
        } else throw new FoodException(UserErrorCode.USER_NOT_FOUND);

        log.info("UserService::deleteByAdmin execution end...");
    }

}
