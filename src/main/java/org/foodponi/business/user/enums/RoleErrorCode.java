package org.foodponi.business.user.enums;

import org.foodponi.core.enums.IErrorCode;

public enum RoleErrorCode implements IErrorCode {

    ROLE_NOT_FOUND("R01", "Role not found"),
    MODULE_PRIVILEGES_NOT_FOUND("R02", "Module or Privileges not found"),;

    RoleErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
