package org.foodponi.business.user.enums;

import org.foodponi.core.enums.IPrivilege;

public enum UserPrivilege implements IPrivilege {

    FIND(1);

    UserPrivilege(int value) {
        this.value = value;
    }

    final int value;

    @Override
    public int value() {
        return this.value;
    }

}
