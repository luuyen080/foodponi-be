package org.foodponi.business.user.enums;

import org.foodponi.core.enums.IErrorCode;

public enum UserErrorCode implements IErrorCode {

    USER_NOT_FOUND("U01", "User not found"),
    USERNAME_OR_PASSWORD_NOT_MATCH("U02", "Username or password not match"),
    CURRENT_PASSWORD_NOT_MATCH("U03", "Current password not match"),
    USER_NOT_FOUND_WITH_GIVEN_USERNAME_OR_EMAIL_OR_PHONE("U04", "User not found with given username or email or phone"),
    EMAIL_OR_PHONE_EXISTED("U05", "Email or phone existed"),
    PHONE_EXIST("U06", "User with phone existed"),
    EMAIL_EXIST("U07", "User with email existed"),
    USERNAME_EXIST("U08", "User with username existed"),
    USER_EXISTED("U09", "User existed"),
    USER_NOT_VERIFIED("U10", "User didn't verify"),
    USER_VERIFIED("U11", "User verified"),
    OTP_NOT_MATCH("U12", "OTP doesn't match"),
    OTP_EXISTED("U13", "OTP existed"),
    USER_BLOCKED("U14", "User blocked"),
    EMAIL_AND_USERNAME_MUST_NOT_BE_NULL("U15", "Email and username must not be null");

    UserErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    final String code;

    final String message;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
