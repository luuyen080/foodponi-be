package org.foodponi.business.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends JpaRepository<User, String> {

    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String username);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);

    Page<User> findAllByStatus(Pageable pageable, boolean userStatus);

    @Query(value = "select * from users where id in (select orders.user_id\n" +
            "                                 from orders\n" +
            "                                          join order_items oi on orders.id = oi.order_id\n" +
            "                                          join product_details pd on pd.id = oi.product_detail_id\n" +
            "                                          join products p on p.id = pd.product_id\n" +
            "                                 where p.user_id = ?1) and users.status = ?2", nativeQuery = true)
    Page<User> findAllByRetailerAndStatus(Pageable pageable, String retailerId, boolean status);

    @Query(value = "select * from users where id in (select orders.user_id\n" +
            "                                 from orders\n" +
            "                                          join order_items oi on orders.id = oi.order_id\n" +
            "                                          join product_details pd on pd.id = oi.product_detail_id\n" +
            "                                          join products p on p.id = pd.product_id\n" +
            "                                 where p.user_id = ?1)", nativeQuery = true)
    Page<User> findAllByRetailer(Pageable pageable, String retailerId);
}
