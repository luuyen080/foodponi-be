package org.foodponi.business.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.*;
import lombok.*;
import org.foodponi.business.address.AddressDTO;
import org.foodponi.business.user.enums.Role;
import org.hibernate.validator.constraints.Length;

public abstract class UserDTO {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class UserIdDTO {

        @NotBlank
        @Size(min = 36, max = 36)
        private String id;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class UserCreationRequestDTO {

        @Length(min = 5, max = 50)
        private String username;

        @Email
        private String email;

        @JsonIgnore
        @Builder.Default
        private Role role = Role.CUSTOMER;

        @NotNull
        @Length(min = 6, max = 50)
        private String password;

        @JsonIgnore
        @Builder.Default
        private boolean status = true;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class UserUpdateInfoRequestDTO {

        @Length(min = 5, max = 50)
        private String username;

        private String biography;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class UserUpdateAvatarRequestDTO {

        private String avatar;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class UserUpdateRoleRequestDTO {

        @NotNull
        @Size(min = 36, max = 36)
        private String id;

        @NotNull
        private Role role;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class ChangePasswordRequestDTO {

        @NotNull
        @Length(min = 6, max = 50)
        private String oldPassword;

        @NotNull
        @Length(min = 6, max = 50)
        private String newPassword;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserResponseDTO {

        private String id;

        private String avatar;

        private String email;

        private String username;

        private String role;

        private boolean status;

        @JsonIgnoreProperties(value = "user")
        private AddressDTO.AddressResponseDTO address;

    }

}
