package org.foodponi.business.user;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.foodponi.business.address.AddressDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(value = "/users")
    public UserDTO.UserResponseDTO create(@RequestBody @Valid UserDTO.UserCreationRequestDTO userCreationRequestDTO) {
        return userService.create(userCreationRequestDTO);
    }

    @GetMapping("/retailer/users")
    public Page<UserDTO.UserResponseDTO> getUsersPageByRetailer(
            Authentication authentication,
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize,
            @RequestParam(required = false) Boolean status) {

        return userService.getUsersPageByRetailer(authentication.getName(), page, pageSize, status);
    }

    @GetMapping("/admin/users")
    public Page<UserDTO.UserResponseDTO> getUsersPageByAdmin(
            @RequestParam(required = false) @Min(value = 0) Integer page,
            @RequestParam(required = false) @Min(value = 1) Integer pageSize,
            @RequestParam(required = false) Boolean status) {

        return userService.getUsersPageByAdmin(page, pageSize, status);
    }

    @GetMapping(value = "/users/{id}")
    public UserDTO.UserResponseDTO getUserById(@PathVariable String id) {
        return userService.getUserById(id);
    }

    @PutMapping(value = "/users/update-avatar")
    public UserDTO.UserResponseDTO updateUserAvatar(
            Authentication authentication,
            @RequestBody @Valid UserDTO.UserUpdateAvatarRequestDTO userUpdateAvatarRequestDTO) {
        return userService.updateUserAvatar(authentication.getName(), userUpdateAvatarRequestDTO);
    }

    @PutMapping(value = "/admin/users/update-role")
    public UserDTO.UserResponseDTO updateRole(
            Authentication authentication,
            @RequestBody @Valid UserDTO.UserUpdateRoleRequestDTO userUpdateRoleRequestDTO) {
        return userService.updateRoleByAdmin(authentication.getName(), userUpdateRoleRequestDTO);
    }

    @PutMapping(value = "/users/update-address")
    public UserDTO.UserResponseDTO updateUserAddress(
            Authentication authentication,
            @RequestBody @Valid AddressDTO.AddressIdDTO addressIdDTO) {
        return userService.updateUserAddress(authentication.getName(), addressIdDTO);
    }

    @PutMapping(value = "/users/change-password")
    public ResponseEntity<?> changePassword(
            Authentication authentication,
            @RequestBody @Valid UserDTO.ChangePasswordRequestDTO changePasswordRequestDTO) {
        userService.changePassword(authentication.getName(), changePasswordRequestDTO);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/admin/users/{id}")
    public ResponseEntity<?> deleteByAdmin(
            Authentication authentication,
            @PathVariable String id) {
        userService.deleteByAdmin(authentication.getName(), id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/users")
    public ResponseEntity<?> delete(Authentication authentication) {
        userService.delete(authentication.getName());
        return ResponseEntity.noContent().build();
    }

}