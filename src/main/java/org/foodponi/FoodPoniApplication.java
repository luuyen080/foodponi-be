package org.foodponi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodPoniApplication {
    public static void main(String[] args) {
        SpringApplication.run(FoodPoniApplication.class, args);
    }

}
